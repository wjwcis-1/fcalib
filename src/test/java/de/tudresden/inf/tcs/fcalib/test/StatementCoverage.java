package de.tudresden.inf.tcs.fcalib.test;


import de.tudresden.inf.tcs.fcaapi.*;
import de.tudresden.inf.tcs.fcaapi.action.ExpertAction;
import de.tudresden.inf.tcs.fcaapi.action.ExpertActionListener;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalContextException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalExpertException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcaapi.utils.IndexedSet;
import de.tudresden.inf.tcs.fcalib.*;
import de.tudresden.inf.tcs.fcalib.action.*;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;
import de.tudresden.inf.tcs.fcalib.change.ObjectHasAttributeChange;
import de.tudresden.inf.tcs.fcalib.utils.ListSet;
import mockit.Deencapsulation;
import mockit.Mock;
import mockit.MockUp;
import org.apache.log4j.BasicConfigurator;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Null;
import org.mockito.internal.stubbing.answers.ThrowsException;

import javax.management.Attribute;
import java.awt.event.ActionEvent;
import java.util.*;

import static org.junit.Assert.*;

public class StatementCoverage {
    @Test
    public void testIllegalAttributeException() {
        IllegalAttributeException illegalAttributeException = new IllegalAttributeException("ABC");
        IllegalAttributeException illegalAttributeException_1 = new IllegalAttributeException();
        assertNotNull(illegalAttributeException.getMessage());
        assertNull(illegalAttributeException_1.getMessage());
    }

    @Test
    public void testIllegalContextException() {
        IllegalContextException illegalContextException = new IllegalContextException("ABC");
        IllegalContextException illegalContextException_1 = new IllegalContextException();
        assertNotNull(illegalContextException);
        assertNotNull(illegalContextException_1);
    }

    @Test
    public void testIllegalExpertException() {
        IllegalExpertException IllegalExpertException = new IllegalExpertException("ABC");
        IllegalExpertException IllegalExpertException_1 = new IllegalExpertException();
        assertNotNull(IllegalExpertException);
        assertNotNull(IllegalExpertException_1);
    }

    @Test
    public void testIllegalObjectException() {
        IllegalObjectException IllegalObjectException = new IllegalObjectException("ABC");
        assertNotNull(IllegalObjectException);
    }

    @Test(expected = NullPointerException.class)
    public void testChangeAttributeOrderAction() {
        ChangeAttributeOrderAction a = new ChangeAttributeOrderAction();
        Object o = new Object();
        int i = 0;
        String command = "";
        ActionEvent actionEvent = new ActionEvent(o, i, command);
        a.actionPerformed(actionEvent);
        assertTrue(true);
    }

    @Test(expected = NullPointerException.class)
    public void testQuestionConfirmedAction() {
        QuestionConfirmedAction a = new QuestionConfirmedAction();
        Object o = new Object();
        int i = 0;
        String command = "";
        ActionEvent actionEvent = new ActionEvent(o, i, command);
        FCAImplication q = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        a.getContext().continueExploration(a.getContext().getNextPremise(q.getPremise()));
        a.setQuestion(q);
        a.actionPerformed(actionEvent);
        assertTrue(true);
    }

    @Test(expected = NullPointerException.class)
    public void testCounterExampleProvidedAction() throws IllegalObjectException {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        FCAImplication implication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FCAObject object = new FCAObject() {
            @Override
            public Object getIdentifier() {
                return new Object();
            }

            @Override
            public String getName() {
                return "1223";
            }

            @Override
            public ObjectDescription getDescription() {
                return new ObjectDescription() {
                    @Override
                    public boolean containsAttribute(Object attribute) {
                        return false;
                    }

                    @Override
                    public boolean containsAttributes(Set attrs) {
                        return false;
                    }

                    @Override
                    public boolean addAttribute(Object attribute) {
                        return false;
                    }

                    @Override
                    public boolean addAttributes(Set attrs) {
                        return false;
                    }

                    @Override
                    public boolean removeAttribute(Object attribute) {
                        return false;
                    }

                    @Override
                    public Object clone() {
                        return null;
                    }
                };
            }

            @Override
            public boolean respects(FCAImplication implication) {
                return true;
            }
        };
        CounterExampleProvidedAction a = new CounterExampleProvidedAction(abstractContext, implication, object);
        Object o = new Object();
        int i = 1;
        String command = "exit";
        ActionEvent actionEvent = new ActionEvent(o, i, command);
        a.actionPerformed(actionEvent);
        assertNull(a.getContext());
        AbstractContext<String, String, FCAObject<String, String>> abstractContext1 = new AbstractContext<String, String, FCAObject<String, String>>() {
            @Override
            public IndexedSet<FCAObject<String, String>> getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(String id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(String attribute, String id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set<String> doublePrime(Set<String> x) {
                return null;
            }

            @Override
            public Set<FCAImplication<String>> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication<String> imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject<String, String> counterExample, FCAImplication<String> imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication<String> implication) {
                return false;
            }

            @Override
            public FCAObject<String, String> getObject(String id) {
                return null;
            }

            @Override
            public FCAObject<String, String> getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject<String, String> object, String attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication<String>> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert<String, String, FCAObject<String, String>> getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert<String, String, FCAObject<String, String>> e) {

            }
        };
        abstractContext1.getObjectCount();
        Set<String> set = new HashSet<>();
        set.add("1");
        set.add("2");
        Set<FCAObject<String, String>> set1 = new HashSet<>();
        set1.add(new FCAObject<String, String>() {
            @Override
            public String getIdentifier() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public ObjectDescription<String> getDescription() {
                return null;
            }

            @Override
            public boolean respects(FCAImplication<String> implication) {
                return false;
            }
        });
        set1.add(null);
        abstractContext1.addAttributes(set);
        abstractContext1.addObjects(set1);
        abstractContext1.getCurrentQuestion();
        abstractContext1.getNextPremise(set);
        abstractContext1.clearObjects();
        Set<String> strings = new HashSet<>();
        strings.add("a1");
        strings.add("a2");
        abstractContext1.addAttributes(strings);
        abstractContext.addAttributes(new Set() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public Object[] toArray(Object[] a) {
                return new Object[0];
            }

            @Override
            public boolean add(Object o) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection c) {
                return false;
            }

            @Override
            public boolean addAll(Collection c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection c) {
                return false;
            }

            @Override
            public void clear() {

            }
        });
    }

    @Test
    public void testResetExplorationAction() {
        BasicConfigurator.configure();
        ResetExplorationAction<String, String, FCAObject<String, String>> a = Mockito.mock(ResetExplorationAction.class);
        a.setContext(new AbstractContext<String, String, FCAObject<String, String>>() {
            @Override
            public IndexedSet<FCAObject<String, String>> getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(String id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(String attribute, String id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set<String> doublePrime(Set<String> x) {
                return null;
            }

            @Override
            public Set<FCAImplication<String>> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication<String> imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject<String, String> counterExample, FCAImplication<String> imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication<String> implication) {
                return false;
            }

            @Override
            public FCAObject<String, String> getObject(String id) {
                return null;
            }

            @Override
            public FCAObject<String, String> getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject<String, String> object, String attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication<String>> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert<String, String, FCAObject<String, String>> getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert<String, String, FCAObject<String, String>> e) {

            }
        });
        BasicConfigurator.configure();
        AbstractContext<String, String, FCAObject<String, String>> context = new FormalContext();
        context.addAttribute("a1");
        context.addAttribute("b1");
        context.addAttribute("c1");
        FullObject<String, String> counterExample = Mockito.mock(FullObject.class);
        FCAImplication<String> fcaImplication = new FCAImplication<String>() {
            @Override
            public Set<String> getPremise() {
                return null;
            }

            @Override
            public Set<String> getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication<String> imp) {
                return false;
            }
        };
        ExpertAction action =
                new CounterExampleProvidedAction(context, fcaImplication, counterExample);
        ResetExplorationAction<String, String, FCAObject<String, String>> a1 = Mockito.mock(ResetExplorationAction.class);
        a1.setContext(context);
        a1.actionPerformed(new ActionEvent(counterExample, 1, "1"));
    }

    @Test
    public void testStopExplorationAction() {
        StopExplorationAction a = Mockito.mock(StopExplorationAction.class);
        Object o = new Object();
        int i = 1;
        String command = "Stop now";
        ActionEvent actionEvent = new ActionEvent(o, i, command);
        a.actionPerformed(actionEvent);
        assertTrue(true);
    }

    @Test
    public void testNewImplicationChange() {
        Context context = new Context() {
            @Override
            public boolean addAttribute(Object attr) throws IllegalAttributeException {
                return false;
            }

            @Override
            public boolean addAttributes(Set attrs) throws IllegalAttributeException {
                return false;
            }

            @Override
            public IndexedSet getAttributes() {
                return null;
            }

            @Override
            public int getAttributeCount() {
                return 0;
            }

            @Override
            public Object getAttributeAtIndex(int index) {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public int getObjectCount() {
                return 0;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addObjects(Set objects) throws IllegalObjectException {
                return false;
            }

            @Override
            public void clearObjects() {

            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public boolean containsObject(Object id) {
                return false;
            }

            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set doublePrime(Set attributes) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public boolean isExpertSet() {
                return false;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }

            @Override
            public Set<FCAImplication> getImplications() {
                return new Set<FCAImplication>() {
                    @Override
                    public int size() {
                        return 1;
                    }

                    @Override
                    public boolean isEmpty() {
                        return false;
                    }

                    @Override
                    public boolean contains(Object o) {
                        return true;
                    }

                    @Override
                    public Iterator<FCAImplication> iterator() {
                        return null;
                    }

                    @Override
                    public Object[] toArray() {
                        return new Object[0];
                    }

                    @Override
                    public <T> T[] toArray(T[] a) {
                        return null;
                    }

                    @Override
                    public boolean add(FCAImplication fcaImplication) {
                        return false;
                    }

                    @Override
                    public boolean remove(Object o) {
                        return false;
                    }

                    @Override
                    public boolean containsAll(Collection<?> c) {
                        return false;
                    }

                    @Override
                    public boolean addAll(Collection<? extends FCAImplication> c) {
                        return false;
                    }

                    @Override
                    public boolean retainAll(Collection<?> c) {
                        return false;
                    }

                    @Override
                    public boolean removeAll(Collection<?> c) {
                        return false;
                    }

                    @Override
                    public void clear() {

                    }
                };
            }

            @Override
            public void expertPerformedAction(ExpertAction action) {

            }
        };
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return true;
            }
        };
        NewImplicationChange newImplicationChange = new NewImplicationChange(context, fcaImplication);
        newImplicationChange.getImplication();
        newImplicationChange.getType();
        newImplicationChange.undo();
        assertTrue(true);
    }

    @Test
    public void testNewObjectChange() {
        Context context = new Context() {
            @Override
            public boolean addAttribute(Object attr) throws IllegalAttributeException {
                return false;
            }

            @Override
            public boolean addAttributes(Set attrs) throws IllegalAttributeException {
                return false;
            }

            @Override
            public IndexedSet getAttributes() {
                return null;
            }

            @Override
            public int getAttributeCount() {
                return 0;
            }

            @Override
            public Object getAttributeAtIndex(int index) {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public int getObjectCount() {
                return 0;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addObjects(Set objects) throws IllegalObjectException {
                return false;
            }

            @Override
            public void clearObjects() {

            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public boolean containsObject(Object id) {
                return false;
            }

            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set doublePrime(Set attributes) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public boolean isExpertSet() {
                return false;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }

            @Override
            public Set<FCAImplication> getImplications() {
                return null;
            }

            @Override
            public void expertPerformedAction(ExpertAction action) {

            }
        };
        FCAObject object = new FCAObject() {
            @Override
            public Object getIdentifier() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public ObjectDescription getDescription() {
                return null;
            }

            @Override
            public boolean respects(FCAImplication implication) {
                return false;
            }
        };
        NewObjectChange newObjectChange = new NewObjectChange(context, object);
        newObjectChange.undo();
        newObjectChange.getType();
        newObjectChange.getObject();
        NewObjectChange newObjectChange_1 = new NewObjectChange(context, null);
        newObjectChange_1.undo();
        assertTrue(true);
    }

    @Test
    public void testObjectHasAttributeChange() {
        FCAObject fcaObject = new FCAObject() {
            @Override
            public Object getIdentifier() {
                return new Object();
            }

            @Override
            public String getName() {
                return "a";
            }

            @Override
            public ObjectDescription getDescription() {
                return new ObjectDescription() {
                    @Override
                    public boolean containsAttribute(Object attribute) {
                        return false;
                    }

                    @Override
                    public boolean containsAttributes(Set attrs) {
                        return false;
                    }

                    @Override
                    public boolean addAttribute(Object attribute) {
                        return false;
                    }

                    @Override
                    public boolean addAttributes(Set attrs) {
                        return false;
                    }

                    @Override
                    public boolean removeAttribute(Object attribute) {
                        return false;
                    }

                    @Override
                    public Object clone() throws CloneNotSupportedException {
                        return null;
                    }
                };
            }

            @Override
            public boolean respects(FCAImplication implication) {
                return true;
            }
        };
        ObjectHasAttributeChange objectHasAttributeChange = new ObjectHasAttributeChange(fcaObject, new Object());
        objectHasAttributeChange.getType();
        objectHasAttributeChange.getObject();
        objectHasAttributeChange.getAttribute();
        objectHasAttributeChange.undo();
        assertTrue(true);
    }

    @Test(expected = NullPointerException.class)
    public void testListSet() {
        ListSet listSet = new ListSet(new Collection() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public Object[] toArray(Object[] a) {
                return new Object[0];
            }

            @Override
            public boolean add(Object o) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection c) {
                return false;
            }

            @Override
            public boolean addAll(Collection c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection c) {
                return false;
            }

            @Override
            public void clear() {

            }
        });
        Object object1 = new Object();
        Object object2 = new Object();
        Object object3 = new Object();
        listSet.add(object1);
        listSet.add(object2);
        listSet.add(object3);
        Collection<Object> collection = new Collection<Object>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Object> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Object o) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }
        };
        collection.add(object1);
        collection.add(object2);
        collection.add(object2);
        collection.add(object3);
        listSet.addAll(collection);
        listSet.contains(null);
        Collection<Object> collection1 = null;
        listSet.containsAll(collection1);
        listSet.clear();
        listSet.toArray();
        listSet.getIndexOf(object3);
        assertTrue(true);
    }

    @Test(expected = NullPointerException.class)
    public void testListSet_V2() {
        Collection<String> collection3 = new ArrayList<>();
        collection3.add("11");
        collection3.add("22");
        collection3.add("33");
        ListSet listSet1 = new ListSet<>();
        ListSet listSet2 = new ListSet<>(collection3);
        listSet2.add(null);
        listSet2.add("11");
        listSet2.add("11");
        listSet2.add("11");
        listSet2.remove("11");
        listSet2.remove("11");
        listSet1.add("a");
        listSet1.add("b");
        listSet1.add("c");
        listSet1.add("d");
        listSet1.add("e");
        Collection<String> collection2 = new ArrayList<>();
        collection2.add("1");
        collection2.add("2");
        collection2.add("3");
        listSet1.addAll(collection2);
        listSet1.addAll(null);
        listSet1.remove("a");
        listSet1.remove("e");
        listSet1.remove("aaaa");
        listSet1.removeAll(collection2);
        listSet1.removeAll(null);
        listSet1.retainAll(collection2);
        listSet1.retainAll(null);
        listSet1.contains(null);
        listSet1.getIndexOf("1");
        listSet1.getIndexOf("10000");
        listSet1.toArray();
        Object[] collection1 = new Object[10];
        listSet1.toArray(collection1);
        listSet1.clear();
        listSet1.toString();
        assertTrue(true);
    }

    @Test(expected = IllegalAttributeException.class)
    public void testFormalContext() throws IllegalObjectException {
        FormalContext formalContext = new FormalContext();
        formalContext.getObject(new Object());
        formalContext.getObject(null);
        formalContext.getConceptLattice();
        formalContext.getExpert();
        formalContext.getConcepts();
        formalContext.getExtents();
        formalContext.getIntents();
        formalContext.refutes(new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        });
        formalContext.allClosures();
        formalContext.isClosed(new Set() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public Object[] toArray(Object[] a) {
                return new Object[0];
            }

            @Override
            public boolean add(Object o) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection c) {
                return false;
            }

            @Override
            public boolean addAll(Collection c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection c) {
                return false;
            }

            @Override
            public void clear() {

            }
        });
        formalContext.getStemBase();
        formalContext.getDuquenneGuiguesBase();
        formalContext.clearObjects();
        //part 1
        BasicConfigurator.configure();
        FormalContext<String, String> context = new FormalContext<>();
        FullObject object1 = new FullObject<>(context);
        FullObject object2 = new FullObject<>(context);
        context.addAttribute("123");
        object2.setName("A12");
        object1.getDescription();
        object1.getIdentifier();
        System.out.println("ID:" + object1.getIdentifier() + "end");
        System.out.println("ID:" + object2.getIdentifier() + "end");
        object1.getName();
        object1.setName("A11");
        object1.getName();
        context.addObject(object1);
        context.getObjectAtIndex(0);
        context.getObject("de.tudresden.inf.tcs.fcalib.FormalContext@1ef7fe8e");
        context.getObject("");
        context.addAttributeToObject("1", "de.tudresden.inf.tcs.fcalib.FormalContext@7a7b0070");
        //part 2
        assertTrue(true);
    }

    @Test(expected = IllegalObjectException.class)
    public void testPartialContext_1() throws IllegalObjectException {
        BasicConfigurator.configure();
        PartialContext partialContext = new PartialContext();
        PartialContext<String, String, PartialObject<String, String>> context = new PartialContext<>();
        NoExpertPartial<String> expert = new NoExpertPartial<>(context);
        PartialObject object = new PartialObject(context);
        context.addAttribute("e");
        context.addAttribute("f");
        context.addAttribute("g");
        context.addAttributeToObject("e", "1");
        context.addObject(object);
        context.doublePrime(new Set<String>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<String> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(String s) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends String> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }
        });
        context.getExpert();
        context.getDuquenneGuiguesBase();
        context.getObjectAtIndex(0);
        context.getObjectCount();
        context.getStemBase();
        partialContext.getStemBase();
        partialContext.getDuquenneGuiguesBase();
        partialContext.refutes(new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        });
        partialContext.doublePrime(new Set() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public Object[] toArray(Object[] a) {
                return new Object[0];
            }

            @Override
            public boolean add(Object o) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection c) {
                return false;
            }

            @Override
            public boolean addAll(Collection c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection c) {
                return false;
            }

            @Override
            public void clear() {

            }
        });
        context.objectHasAttribute(object, "1");
        context.removeAttributeFromObject("3", "1");
        expert.addExpertActionListener(context);
        context.setExpert(expert);
        StartExplorationAction<String, String, PartialObject<String, String>> action = new StartExplorationAction<>();
        action.setContext(context);
        expert.fireExpertAction(action);
        assertNull(partialContext.getStemBase());
    }

    @Test
    public void testImplicationSet() {
        ImplicationSet implicationSet = new ImplicationSet(new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        });
        assertNotNull(implicationSet.allClosures());
        assertNotNull(implicationSet.getContext());
        ImplicationSet<String> implicationSet1 = Mockito.mock(ImplicationSet.class);
        FCAImplication implication1 = Mockito.mock(FCAImplication.class);
        FCAImplication<String> implication2 = new FCAImplication<String>() {
            @Override
            public Set<String> getPremise() {
                return null;
            }

            @Override
            public Set<String> getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication<String> imp) {
                return false;
            }
        };
        implicationSet1.add(implication1);
        implicationSet1.add(implication2);
        Set<String> set = new HashSet<>();
        set.add("a");
        set.add("b");
        implicationSet1.closure(set);
        implicationSet1.isClosed(set);
    }

    @Test(expected = NullPointerException.class)
    public void testFCAImplication() {
        QuestionConfirmedAction questionConfirmedAction = new QuestionConfirmedAction() {
        };
        int uniqueId = (int) System.currentTimeMillis();
        String commandName = "";
        ActionEvent actionEvent = new ActionEvent(this, uniqueId, commandName);
        questionConfirmedAction.setQuestion(null);
        Set<String> strings = new HashSet<>();
        strings.add("123");
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return strings;
            }

            @Override
            public Set getConclusion() {
                return strings;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return true;
            }
        };
        fcaImplication.getConclusion();
        fcaImplication.equals(null);
        fcaImplication.getPremise();
        assertNull(questionConfirmedAction.getContext());
        questionConfirmedAction.setQuestion(fcaImplication);
        questionConfirmedAction.actionPerformed(actionEvent);
    }

    @Test
    public void testFullObjectDescription() throws CloneNotSupportedException {
        FullObjectDescription fullObjectDescription = new FullObjectDescription();
        Set<Attribute> set = new HashSet<>();
        set.add(new Attribute("1", new Object()));
        set.add(new Attribute("2", new Object()));
        set.add(new Attribute("3", new Object()));
        fullObjectDescription.addAttribute(new Attribute("123", new Object()));
        fullObjectDescription.addAttributes(set);
        fullObjectDescription.removeAttribute(new Attribute("123", new Object()));
        assertNotNull(fullObjectDescription.clone());
    }

    @Test
    public void testExpertAction() {
        int uniqueId = (int) System.currentTimeMillis();
        String commandName = "";
        ActionEvent actionEvent = new ActionEvent(this, uniqueId, commandName);
        ExpertAction expertAction = new ExpertAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }

            @Override
            public Context<?, ?, ?> getContext() {
                return null;
            }
        };
        expertAction.actionPerformed(actionEvent);
    }

    @Test
    public void testPartialObject() {
        Set<String> strings = new HashSet<>();
        strings.add("1");
        strings.add("2");
        PartialObject partialObject1 = new PartialObject("123", strings);
        partialObject1.getDescription();
        partialObject1.setName("q");
        partialObject1.getName();
        FCAImplication<String> implication = Mockito.mock(FCAImplication.class);
        partialObject1.refutes(implication);
        assertNotNull(partialObject1.getDescription());
        Set<String> string2 = new HashSet<>();
        string2.add("a");
        string2.add("b");
        PartialObject partialObject2 = new PartialObject("123", strings, string2);
        assertNotNull(partialObject2.getDescription());
    }

    @Test
    public void testImplication() {
        Implication implication1 = new Implication();
        Implication implication2 = new Implication();
        assertTrue(implication1.equals(implication2));
    }

    @Test
    public void testFullObject() throws IllegalObjectException {
        BasicConfigurator.configure();
        PartialContext<String, String, PartialObject<String, String>> context = new PartialContext<>();
        FCAImplication<String> fcaImplication = Mockito.mock(FCAImplication.class);
        context.refutes(fcaImplication);
        PartialObject<String, String> object1 = Mockito.mock(PartialObject.class);
        object1.setName("a1");
        object1.respects(fcaImplication);
        context.addObject(object1);
        context.getDuquenneGuiguesBase();
        context.getStemBase();
        assertEquals(context.getObjectCount(), 1);
        context.addAttribute("1");
        context.getObjectAtIndex(0);
        context.removeObject(object1);
        context.clearObjects();
        PartialObject<String, String> object2 = Mockito.mock(PartialObject.class);
        object2.refutes(fcaImplication);
        context.addObject(object2);
        assertFalse(context.refutes(fcaImplication));
    }

    @Test
    public void testFormalContext_1() throws IllegalObjectException {
        FormalContext<String, String> formalContext = new FormalContext<>();
        FullObject<String, String> object1 = Mockito.mock(FullObject.class);
        formalContext.addObject(object1);
        formalContext.removeObject(object1);
        formalContext.addObject(object1);
    }

    @Test
    public void testAbstractExpert() {
        AbstractExpert<String, String, FCAObject<String, String>> abstractExpert1 = new AbstractExpert<String, String, FCAObject<String, String>>() {
            @Override
            public void requestCounterExample(FCAImplication<String> question) {

            }

            @Override
            public void askQuestion(FCAImplication<String> question) {

            }

            @Override
            public void counterExampleInvalid(FCAObject<String, String> counterExample, int reason) {

            }

            @Override
            public void forceToCounterExample(FCAImplication<String> implication) {

            }

            @Override
            public void explorationFinished() {

            }

            @Override
            public void implicationFollowsFromBackgroundKnowledge(FCAImplication<String> implication) {

            }
        };
        ExpertActionListener<String, String> listener = new ExpertActionListener<String, String>() {
            @Override
            public void expertPerformedAction(ExpertAction action) {

            }
        };
        abstractExpert1.addExpertActionListener(listener);
        abstractExpert1.removeExpertActionListeners();
    }

    @Test(expected = IllegalAttributeException.class)
    public void test1() {
        AbstractContext<String, String, FCAObject<String, String>> abstractContext = new AbstractContext<String, String, FCAObject<String, String>>() {
            @Override
            public IndexedSet<FCAObject<String, String>> getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(String id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(String attribute, String id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set<String> doublePrime(Set<String> x) {
                return null;
            }

            @Override
            public Set<FCAImplication<String>> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication<String> imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject<String, String> counterExample, FCAImplication<String> imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication<String> implication) {
                return false;
            }

            @Override
            public FCAObject<String, String> getObject(String id) {
                return null;
            }

            @Override
            public FCAObject<String, String> getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject<String, String> object, String attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication<String>> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert<String, String, FCAObject<String, String>> getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert<String, String, FCAObject<String, String>> e) {

            }
        };
        abstractContext.addAttribute("1");
        abstractContext.addAttribute("1");
        Set<String> att = new HashSet<>();
        att.add("2");
        att.add("3");
        assertNotNull(abstractContext.addAttributes(att));
    }

    @Test
    public void test2() {
        AbstractContext<String, String, FCAObject<String, String>> abstractContext = new AbstractContext<String, String, FCAObject<String, String>>() {
            @Override
            public IndexedSet<FCAObject<String, String>> getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(String id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(String attribute, String id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set<String> doublePrime(Set<String> x) {
                return null;
            }

            @Override
            public Set<FCAImplication<String>> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication<String> imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject<String, String> counterExample, FCAImplication<String> imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication<String> implication) {
                return false;
            }

            @Override
            public FCAObject<String, String> getObject(String id) {
                return null;
            }

            @Override
            public FCAObject<String, String> getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject<String, String> object, String attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication<String>> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert<String, String, FCAObject<String, String>> getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert<String, String, FCAObject<String, String>> e) {

            }
        };
        Set<String> att = new HashSet<>();
        att.add("2");
        att.add("3");
        abstractContext.addAttributes(att);
    }

    @Test
    public void test3() throws IllegalObjectException {
        AbstractContext<String, String, FCAObject<String, String>> abstractContext = new AbstractContext<String, String, FCAObject<String, String>>() {
            @Override
            public IndexedSet<FCAObject<String, String>> getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(String id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(String attribute, String id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set<String> doublePrime(Set<String> x) {
                return null;
            }

            @Override
            public Set<FCAImplication<String>> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication<String> imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject<String, String> counterExample, FCAImplication<String> imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication<String> implication) {
                return false;
            }

            @Override
            public FCAObject<String, String> getObject(String id) {
                return null;
            }

            @Override
            public FCAObject<String, String> getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject<String, String> object, String attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication<String>> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert<String, String, FCAObject<String, String>> getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert<String, String, FCAObject<String, String>> e) {

            }
        };
        Set<FCAObject<String, String>> att = new HashSet<>();
        att.add(new FCAObject<String, String>() {
            @Override
            public String getIdentifier() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public ObjectDescription<String> getDescription() {
                return null;
            }

            @Override
            public boolean respects(FCAImplication<String> implication) {
                return false;
            }
        });
        att.add(new FCAObject<String, String>() {
            @Override
            public String getIdentifier() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public ObjectDescription<String> getDescription() {
                return null;
            }

            @Override
            public boolean respects(FCAImplication<String> implication) {
                return false;
            }
        });
        abstractContext.addObjects(att);
    }

    @Test
    public void test4() {
        PartialObjectDescription<String> description = new PartialObjectDescription<>();
        description.addAttribute("1");
    }

    @Test
    public void test5() {
        PartialObjectDescription<String> description = new PartialObjectDescription<>();
        description.addAttribute("1");
        description.containsNegatedAttribute("1");
        description.clone();
    }

    @Test
    public void test6() {
        PartialObjectDescription<String> description = new PartialObjectDescription<>();
        Set<String> strings = new HashSet<>();
        strings.add("1");
        strings.add("2");
        description.addNegatedAttribute("1");
        description.containsNegatedAttributes(strings);
        description.clone();
    }

    @Test(expected = NullPointerException.class)
    public void test7() {
        ListSet<String> strings = new ListSet<>();
        strings.toString();
        strings.toArray();
        strings.contains(null);
        Collection<String> collection = new HashSet<>();
        collection.add("1");
        collection.add("2");
        strings.addAll(collection);
        strings.getIndexOf("1");
    }


    @Test
    public void test8() {
        BasicConfigurator.configure();
        FormalContext<String, String> context = new FormalContext<>();
        NoExpertFull<String> expert = new NoExpertFull<>(context);
        context.addAttribute("a");
        context.addAttribute("b");
        context.addAttribute("c");
        expert.addExpertActionListener(context);
        context.setExpert(expert);
        StartExplorationAction<String, String, FullObject<String, String>> action = new StartExplorationAction<>();
        action.setContext(context);
        expert.fireExpertAction(action);
        StopExplorationAction<String, String, FullObject<String, String>> action1 = new StopExplorationAction<>();
        expert.fireExpertAction(action1);
        QuestionRejectedAction<String, String, FullObject<String, String>> action2 = new QuestionRejectedAction<>();
        action2.setQuestion(null);
        action2.getQuestion();
    }

    @Test(expected = NullPointerException.class)
    public void test9() {
        ListSet<Byte> set = new ListSet<>();
        Byte b1 = 12;
        Byte b2 = 12;
        Byte b3 = 12;
        Byte b4 = 12;
        Byte b5 = 5;
        set.add(b1);
        set.add(b2);
        set.add(b3);
        set.add(b4);
        Collection<Byte> collection = new HashSet<>();
        collection.add(b5);
        assertFalse(set.containsAll(collection));
        set.remove(3);
        set.getIndexOf(b2);
        set.getIndexOf(b5);
        Byte[] B = new Byte[10];
        B[1] = 0;
        set.toArray(B);
        set.removeAll(collection);
        set.retainAll(collection);
        set.iterator().remove();
        set.remove(null);
    }

    @Test
    public void test10() {
        ChangeAttributeOrderAction<String, String, FCAObject<String, String>> action1 = new ChangeAttributeOrderAction<>();
        action1.putValue("A1", new Object());
        AbstractContext<String, String, FCAObject<String, String>> context = new AbstractContext<String, String, FCAObject<String, String>>() {
            @Override
            public IndexedSet<FCAObject<String, String>> getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(String id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(String attribute, String id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set<String> doublePrime(Set<String> x) {
                return null;
            }

            @Override
            public Set<FCAImplication<String>> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication<String> imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject<String, String> counterExample, FCAImplication<String> imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication<String> implication) {
                return false;
            }

            @Override
            public FCAObject<String, String> getObject(String id) {
                return null;
            }

            @Override
            public FCAObject<String, String> getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject<String, String> object, String attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication<String>> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert<String, String, FCAObject<String, String>> getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert<String, String, FCAObject<String, String>> e) {

            }
        };
        context.addAttribute("1");
        context.addAttribute("2");
        context.addAttribute("3");
        action1.setContext(context);
        action1.actionPerformed(new ActionEvent(new Object(), 0, "A1"));
    }

    @Test(expected = NullPointerException.class)
    public void test11() {
        AbstractContext<String, String, FCAObject<String, String>> context = new AbstractContext<String, String, FCAObject<String, String>>() {
            @Override
            public IndexedSet<FCAObject<String, String>> getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(String id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(String attribute, String id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set<String> doublePrime(Set<String> x) {
                return null;
            }

            @Override
            public Set<FCAImplication<String>> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication<String> imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject<String, String> counterExample, FCAImplication<String> imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication<String> implication) {
                return false;
            }

            @Override
            public FCAObject<String, String> getObject(String id) {
                return null;
            }

            @Override
            public FCAObject<String, String> getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject<String, String> object, String attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication<String>> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert<String, String, FCAObject<String, String>> getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert<String, String, FCAObject<String, String>> e) {

            }
        };
        FCAImplication<String> implication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        implication.getPremise().add("1");
        implication.getPremise().add("2");
        FCAObject<String, String> object = new FCAObject() {
            @Override
            public Object getIdentifier() {
                return new Object();
            }

            @Override
            public String getName() {
                return "1223";
            }

            @Override
            public ObjectDescription getDescription() {
                return new ObjectDescription() {
                    @Override
                    public boolean containsAttribute(Object attribute) {
                        return true;
                    }

                    @Override
                    public boolean containsAttributes(Set attrs) {
                        return true;
                    }

                    @Override
                    public boolean addAttribute(Object attribute) {
                        return true;
                    }

                    @Override
                    public boolean addAttributes(Set attrs) {
                        return true;
                    }

                    @Override
                    public boolean removeAttribute(Object attribute) {
                        return true;
                    }

                    @Override
                    public Object clone() {
                        return new Object();
                    }
                };
            }

            @Override
            public boolean respects(FCAImplication implication) {
                return true;
            }
        };
        object.respects(implication);
        CounterExampleProvidedAction action1 = new CounterExampleProvidedAction<>(context, implication, object);
        action1.putValue("A1", new Object());
        action1.setContext(context);
        action1.actionPerformed(new ActionEvent(new Object(), 0, "A1"));
    }

    @Test
    public void test12() {
        StartExplorationAction<String, String, FCAObject<String, String>> action1 = new StartExplorationAction<>();
        action1.putValue("A1", new Object());
        AbstractContext<String, String, FCAObject<String, String>> context = new AbstractContext<String, String, FCAObject<String, String>>() {
            @Override
            public IndexedSet<FCAObject<String, String>> getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(String id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(String attribute, String id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set<String> doublePrime(Set<String> x) {
                return null;
            }

            @Override
            public Set<FCAImplication<String>> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication<String> imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject<String, String> counterExample, FCAImplication<String> imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication<String> implication) {
                return false;
            }

            @Override
            public FCAObject<String, String> getObject(String id) {
                return null;
            }

            @Override
            public FCAObject<String, String> getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject<String, String> object, String attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication<String>> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert<String, String, FCAObject<String, String>> getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert<String, String, FCAObject<String, String>> e) {

            }
        };
        context.addAttribute("1");
        context.addAttribute("2");
        context.addAttribute("3");
        action1.setContext(context);
        action1.actionPerformed(new ActionEvent(new Object(), 100, "add"));
    }

    @Test
    public void test13() throws IllegalObjectException, CloneNotSupportedException {
        AbstractContext<String, String, FCAObject<String, String>> context = new AbstractContext<String, String, FCAObject<String, String>>() {
            @Override
            public IndexedSet<FCAObject<String, String>> getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(String id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(String attribute, String id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set<String> doublePrime(Set<String> x) {
                return null;
            }

            @Override
            public Set<FCAImplication<String>> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication<String> imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject<String, String> counterExample, FCAImplication<String> imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication<String> implication) {
                return false;
            }

            @Override
            public FCAObject<String, String> getObject(String id) {
                return null;
            }

            @Override
            public FCAObject<String, String> getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject<String, String> object, String attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication<String>> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert<String, String, FCAObject<String, String>> getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert<String, String, FCAObject<String, String>> e) {

            }
        };
        context.addAttribute("1");
        context.addAttribute("2");
        context.addAttribute("3");
        Implication<String> implications = new Implication<>();
        implications.getPremise().add("1");
        implications.getPremise().add("2");
        FCAObject<String, String> object = new FCAObject<String, String>() {
            @Override
            public String getIdentifier() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public ObjectDescription<String> getDescription() {
                ObjectDescription<String> stringObjectDescription = new ObjectDescription<String>() {
                    @Override
                    public boolean containsAttribute(String attribute) {
                        return false;
                    }

                    @Override
                    public boolean containsAttributes(Set<String> attrs) {
                        return false;
                    }

                    @Override
                    public boolean addAttribute(String attribute) {
                        return false;
                    }

                    @Override
                    public boolean addAttributes(Set<String> attrs) {
                        return false;
                    }

                    @Override
                    public boolean removeAttribute(String attribute) {
                        return false;
                    }

                    @Override
                    public Object clone() throws CloneNotSupportedException {
                        return null;
                    }
                };
                stringObjectDescription.addAttribute("1");
                return stringObjectDescription;
            }

            @Override
            public boolean respects(FCAImplication<String> implication) {
                return false;
            }
        };
        object.respects(implications);
        object.getDescription().addAttribute("a1");
        context.addObject(object);
        context.getImplications();
        context.getCurrentQuestion();
    }

    @Test(expected = IllegalObjectException.class)
    public void test14() throws IllegalObjectException {
        AbstractContext<String, String, FCAObject<String, String>> context = new AbstractContext<String, String, FCAObject<String, String>>() {
            @Override
            public IndexedSet<FCAObject<String, String>> getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(String id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(String attribute, String id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set<String> doublePrime(Set<String> x) {
                return null;
            }

            @Override
            public Set<FCAImplication<String>> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication<String> imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject<String, String> counterExample, FCAImplication<String> imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication<String> implication) {
                return false;
            }

            @Override
            public FCAObject<String, String> getObject(String id) {
                return null;
            }

            @Override
            public FCAObject<String, String> getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject<String, String> object, String attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication<String>> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert<String, String, FCAObject<String, String>> getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert<String, String, FCAObject<String, String>> e) {

            }
        };
        context.addAttribute("1");
        context.addAttribute("2");
        context.addAttribute("3");
        ImplicationSet<String> set = new ImplicationSet<>(context);
        Implication<String> implications = new Implication<>();
        implications.getPremise().add("1");
        implications.getPremise().add("2");
        set.add(implications);

        PartialContext<String, String, PartialObject<String, String>> context1 = new PartialContext<>();
        PartialObject object = new PartialObject(context1);
        object.setName("1");
        object.getDescription().addAttribute("1");
        context1.addObject(object);
        context1.getObject(object.getDescription().toString());
        context1.removeObject(object);
        context1.removeObject(object);
    }

    @Test
    public void test15() {
        PartialContext<String, String, PartialObject<String, String>> context1 = new PartialContext<>();
        PartialObject object = new PartialObject(context1);
        object.setName("1");
        object.getDescription().addAttribute("1");
        context1.addObject(object);
        context1.getObject(object.getDescription().toString());
        Implication<String> implication = new Implication<>();
        implication.getPremise().add("a");
        implication.getConclusion().add("b");
        context1.refutes(implication);
        context1.addObject(object);
        context1.addObject(object);
    }

    @Test(expected = IllegalObjectException.class)
    public void test16() throws IllegalObjectException {
        FormalContext<String, String> context = new FormalContext<>();
        context.addAttribute("1");
        FullObject<String, String> object1 = new FullObject<>("1");
        ;
        object1.setName("a1");
        FullObject<String, String> object2 = new FullObject<>("2");
        ;
        object2.setName("a2");
        context.addObject(object1);
        context.addObject(object1);
    }

    @Test
    public void test17() throws IllegalObjectException {
        FormalContext<String, String> context = new FormalContext<>();
        context.addAttribute("A1");
        FullObject<String, String> object1 = new FullObject<>("1");
        object1.setName("a1");
        FullObject<String, String> object2 = new FullObject<>("2");
        object2.setName("a2");
        context.addObject(object1);
        context.getObject(object1.getIdentifier());
        context.addAttributeToObject("A1", object1.getIdentifier());
        context.removeObject(object1.getIdentifier());
    }

    @Test(expected = IllegalObjectException.class)
    public void test18() throws IllegalObjectException {
        FormalContext<String, String> context = new FormalContext<>();
        context.addAttribute("1");
        FullObject<String, String> object1 = new FullObject<>("1");
        ;
        object1.setName("a1");
        object1.getDescription().addAttribute("ks1");
        FullObject<String, String> object2 = new FullObject<>("2");
        ;
        object2.setName("a2");
        context.addObject(object1);
        context.getObject(object1.getIdentifier());
        context.objectHasAttribute(object1, "ks1");
        context.removeObject(object1);
        context.removeObject(object1);
    }

    @Test
    public void test19() throws IllegalObjectException {
        FormalContext<String, String> context = new FormalContext<>();
        context.addAttribute("1");
        FullObject<String, String> object1 = new FullObject<>("1");
        ;
        object1.setName("a1");
        object1.getDescription().addAttribute("ks1");
        context.addObject(object1);
        Implication<String> implication1 = new Implication<>();
        implication1.getConclusion().add("0");
        implication1.getPremise().add("00");
        Implication<String> implication2 = new Implication<>();
        implication1.getConclusion().add("1");
        implication1.getPremise().add("11");
        context.followsFromBackgroundKnowledge(implication2);
        object1.refutes(implication1);
        context.refutes(implication1);
    }

    @Test(expected = IllegalObjectException.class)
    public void test20() throws IllegalObjectException {
        FormalContext<String, String> context = new FormalContext<>();
        context.addAttribute("A1");
        FullObject<String, String> object1 = new FullObject<>("1");
        object1.setName("a1");
        FullObject<String, String> object2 = new FullObject<>("2");
        object2.setName("a2");
        context.addObject(object1);
        context.getObject(object1.getIdentifier());
        context.addAttributeToObject("A1", object2.getIdentifier());
    }

    @Test(expected = IllegalAttributeException.class)
    public void test21() throws IllegalObjectException {
        FormalContext<String, String> context = new FormalContext<>();
        context.addAttribute("A1");
        FullObject<String, String> object1 = new FullObject<>("1");
        object1.setName("a1");
        context.addObject(object1);
        context.getObject(object1.getIdentifier());
        context.addAttributeToObject("A1", object1.getIdentifier());
        context.addAttributeToObject("A1", object1.getIdentifier());
    }

    @Test
    public void test22() throws IllegalObjectException {
        FormalContext<String, String> context = new FormalContext<>();
        context.addAttribute("A1");
        FullObject<String, String> object1 = new FullObject<>("1");
        object1.setName("a1");
        context.addObject(object1);
        context.addAttributeToObject("A1", object1.getIdentifier());
        context.removeAttributeFromObject("A1", object1.getIdentifier());
    }

    @Test(expected = IllegalAttributeException.class)
    public void test23() throws IllegalObjectException {
        FormalContext<String, String> context = new FormalContext<>();
        context.addAttribute("A1");
        FullObject<String, String> object1 = new FullObject<>("1");
        object1.setName("a1");
        context.addObject(object1);
        context.addAttributeToObject("A1", object1.getIdentifier());
        context.removeAttributeFromObject("A2", object1.getIdentifier());
    }

    @Test(expected = IllegalObjectException.class)
    public void test24() throws IllegalObjectException {
        FormalContext<String, String> context = new FormalContext<>();
        context.addAttribute("A1");
        FullObject<String, String> object1 = new FullObject<>("1");
        object1.setName("a1");
        FullObject<String, String> object2 = new FullObject<>("2");
        object2.setName("a2");
        context.addObject(object1);
        context.addAttributeToObject("A1", object1.getIdentifier());
        context.removeAttributeFromObject("A1", object2.getIdentifier());
    }

    @Test(expected = IllegalAttributeException.class)
    public void test25() throws IllegalObjectException {
        FormalContext<String, String> context = new FormalContext<>();
        context.addAttribute("A1");
        FullObject<String, String> object1 = new FullObject<>("1");
        object1.setName("a1");
        object1.getDescription().addAttribute("B1");
        context.addObject(object1);
        context.removeAttributeFromObject("A1", object1.getIdentifier());
    }

    @Test(expected = IllegalAttributeException.class)
    public void test26() throws IllegalObjectException {
        PartialContext<String, String, PartialObject<String, String>> context = new PartialContext<>();
        PartialObject<String, String> object1 = new PartialObject<>("1");
        object1.setName("K1");
        context.addObject(object1);
        context.removeObject(object1.getIdentifier());
        context.addAttribute("A1");
        context.addAttributeToObject("A2", object1.getIdentifier());
    }

    @Test(expected = IllegalAttributeException.class)
    public void test27() throws IllegalObjectException {
        PartialContext<String, String, PartialObject<String, String>> context = new PartialContext<>();
        PartialObject<String, String> object1 = new PartialObject<>("1");
        object1.setName("K1");
        context.addObject(object1);
        context.addAttribute("A1");
        context.addAttributeToObject("A1", object1.getIdentifier());
        context.addAttributeToObject("A1", object1.getIdentifier());
    }

    @Test(expected = IllegalAttributeException.class)
    public void test28() throws IllegalObjectException {
        PartialContext<String, String, PartialObject<String, String>> context = new PartialContext<>();
        PartialObject<String, String> object1 = new PartialObject<>("1");
        object1.setName("K1");
        context.addObject(object1);
        object1.getDescription().addAttribute("A1");
        context.removeAttributeFromObject("A1", object1.getIdentifier());
    }

    @Test(expected = IllegalAttributeException.class)
    public void test29() throws IllegalObjectException {
        PartialContext<String, String, PartialObject<String, String>> context = new PartialContext<>();
        PartialObject<String, String> object1 = new PartialObject<>("1");
        object1.setName("K1");
        context.addObject(object1);
        context.addAttribute("A1");
        context.removeAttributeFromObject("A1", object1.getIdentifier());
    }

    @Test(expected = IllegalObjectException.class)
    public void test30() throws IllegalObjectException {
        PartialContext<String, String, PartialObject<String, String>> context = new PartialContext<>();
        PartialObject<String, String> object1 = new PartialObject<>("1");
        object1.setName("K1");
        PartialObject<String, String> object2 = new PartialObject<>("2");
        object1.setName("K3");
        context.addObject(object1);
        context.addAttribute("A1");
        context.addAttributeToObject("A1", object1.getIdentifier());
        context.removeAttributeFromObject("A1", object2.getIdentifier());
    }

    @Test
    public void test31() throws IllegalObjectException {
        PartialContext<String, String, PartialObject<String, String>> context = new PartialContext<>();
        PartialObject<String, String> object1 = new PartialObject<>("1");
        object1.setName("K1");
        context.addObject(object1);
        context.addAttribute("A1");
        context.addAttributeToObject("A1", object1.getIdentifier());
        assertTrue(context.objectHasAttribute(object1, "A1"));
        assertFalse(context.objectHasNegatedAttribute(object1, "A1"));
        context.removeAttributeFromObject("A1", object1.getIdentifier());
    }

    @Test(expected = IllegalArgumentException.class)
    public void test32() {
        PartialObjectDescription<String> description = new PartialObjectDescription();
        description.addNegatedAttribute("A1");
        description.addAttribute("A1");
    }

    @Test(expected = IllegalAttributeException.class)
    public void test33() {
        PartialObjectDescription<String> description = new PartialObjectDescription();
        description.addAttribute("A1");
        description.addNegatedAttribute("A1");
    }

    /*
    mutation
     */
    @Test(expected = NullPointerException.class)
    public void testAbstractContext_getObjectCount() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        abstractContext.getObjectCount();
    }

    @Test(expected = IllegalAttributeException.class)
    public void testAbstractContext_addAttributes() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        Set<String> att = new HashSet<>();
        att.add("1");
        att.add("2");
        assertTrue(abstractContext.addAttributes(att));
        abstractContext.addAttributes(att);
    }

    @Test
    public void testAbstractContext_addObjects() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        Set<Object> object = new HashSet<>();
        try {
            abstractContext.addObjects(object);
        } catch (Exception IllegalObjcetException) {
        }
    }

    @Test(expected = NullPointerException.class)
    public void testAbstractContext_clearObjects() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        abstractContext.clearObjects();
    }

    @Test(expected = NullPointerException.class)
    public void testAbstractContext_getNextPremise() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        abstractContext.getNextPremise(null);
    }

    @Test(expected = NullPointerException.class)
    public void testAbstractContext_expertPerformedAction() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        abstractContext.expertPerformedAction(null);
    }

    @Test(expected = NullPointerException.class)
    public void testAbstractContext_continueExploration() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        Implication<String> implication = new Implication<>();
        implication.getPremise().add("a");
        implication.getPremise().add("b");
        abstractContext.continueExploration(implication.getPremise());
        abstractContext.continueExploration(null);
    }


    @Test //(expected = NullPointerException.class)
    public void testAbstractExpert_removeExpertActionListeners() {
        AbstractExpert abstractExpert = new AbstractExpert() {
            @Override
            public void requestCounterExample(FCAImplication question) {

            }

            @Override
            public void askQuestion(FCAImplication question) {

            }

            @Override
            public void counterExampleInvalid(FCAObject counterExample, int reason) {

            }

            @Override
            public void forceToCounterExample(FCAImplication implication) {

            }

            @Override
            public void explorationFinished() {

            }

            @Override
            public void implicationFollowsFromBackgroundKnowledge(FCAImplication implication) {

            }
        };
        abstractExpert.removeExpertActionListeners();
    }

    @Test //(expected = NullPointerException.class)
    public void testAbstractExpert_fireExpertAction() {
        AbstractExpert abstractExpert = new AbstractExpert() {
            @Override
            public void requestCounterExample(FCAImplication question) {

            }

            @Override
            public void askQuestion(FCAImplication question) {

            }

            @Override
            public void counterExampleInvalid(FCAObject counterExample, int reason) {

            }

            @Override
            public void forceToCounterExample(FCAImplication implication) {

            }

            @Override
            public void explorationFinished() {

            }

            @Override
            public void implicationFollowsFromBackgroundKnowledge(FCAImplication implication) {

            }
        };
        abstractExpert.fireExpertAction(null);
    }


    @Test
    public void testFormalContext_addObject() {
        FormalContext formalContext = new FormalContext();
        Set<Object> object = new HashSet<>();
        try {
            formalContext.addObjects(object);
        } catch (Exception IllegalObjcetException) {
        }
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testFormalContext_getObjectAtIndex() {
        FormalContext formalContext = new FormalContext();
        formalContext.getObjectAtIndex(-1);
    }

    @Test
    public void testFormalContext_removeObject() {
        FormalContext formalContext = new FormalContext();
        try{
            formalContext.removeObject(null);
        }catch (Exception IllegalObjcetException) {
        }
    }

    @Test
    public void testFormalContext_addAttributeToObject() {
        FormalContext formalContext = new FormalContext();
        try{
            formalContext.addAttributeToObject("1",null);
        }catch (Exception IllegalObjcetException) {
        }
    }

    @Test
    public void testFormalContext_removeAttributeFromObject() {
        FormalContext formalContext = new FormalContext();
        try{
            formalContext.removeAttributeFromObject("1",null);
        }catch (Exception IllegalObjcetException) {
        }
    }

    @Test
    public void testFormalContext_objectHasAttribute() {
        FormalContext formalContext = new FormalContext();
        //FullObject<String, String> object1 = new FullObject<>("1");
        try{
            formalContext.objectHasAttribute(null,"1");
        }catch (Exception IllegalObjcetException) {
        }
    }

    @Test
    public void testFormalContext_objectHasAttribute2() {
        FormalContext formalContext = new FormalContext();
        FullObject<String, String> object = new FullObject<>("1");
        try{
            formalContext.objectHasAttribute(object,null);
        }catch (Exception IllegalObjcetException) {
        }
    }

    @Test
    public void testFormalContext_closure() {
        FormalContext formalContext = new FormalContext();
        Set<String> set = new HashSet<>();
        assertFalse(formalContext.closure(null) == formalContext.closure(set));
    }

    @Test (expected = NullPointerException.class)
    public void testFormalContext_isclosed() {
        FormalContext formalContext = new FormalContext();
        Set<String> set = new HashSet<>();
        assertTrue(formalContext.isClosed(set));
        formalContext.isClosed(null);
    }

    @Test
    public void testFormalContext_refutes() {
        FormalContext formalContext = new FormalContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        assertFalse(formalContext.refutes(fcaImplication));
        assertFalse(formalContext.refutes(null));
    }

    @Test
    public void testFormalContext_followsFromBackgroundKnowledge() {
        FormalContext formalContext = new FormalContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        assertFalse(formalContext.followsFromBackgroundKnowledge(null));
    }

    @Test (expected = NullPointerException.class)
    public void testFullObject_refutes() {
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FullObject fullObject = new FullObject(fcaImplication);
        fullObject.refutes(null);
    }

    @Test
    public void testFullObjectDescription_addAttribute() {
        FullObjectDescription fullObjectDescription = new FullObjectDescription();
        fullObjectDescription.addAttribute("1");
        assertFalse(fullObjectDescription.addAttribute("1"));
        assertTrue(fullObjectDescription.addAttribute(null));
    }

    @Test
    public void testFullObjectDescription_addAttributes() {
        FullObjectDescription fullObjectDescription = new FullObjectDescription();
        fullObjectDescription.addAttribute("1");
        fullObjectDescription.addAttribute("2");
        Set<String> set = new HashSet<>();
        set.add("12");
        assertTrue(fullObjectDescription.addAttributes(set));
    }

    @Test
    public void testFullObjectDescription_removeAttribute() {
        FullObjectDescription fullObjectDescription = new FullObjectDescription();
        fullObjectDescription.addAttribute("1");
        assertTrue(fullObjectDescription.removeAttribute("1"));
        assertFalse(fullObjectDescription.removeAttribute("2"));
    }

    @Test (expected = NullPointerException.class)
    public void testImplicationSet_add() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        ImplicationSet implicationSet = new ImplicationSet(abstractContext);
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        assertTrue(implicationSet.add(fcaImplication));
        assertFalse(implicationSet.add(fcaImplication));
        implicationSet.add(null);
    }

    @Test (expected = NullPointerException.class)
    public void testImplicationSet_closure() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        ImplicationSet implicationSet = new ImplicationSet(abstractContext);
        Set<String> set = new HashSet<>();
        assertFalse(implicationSet.closure(null) == implicationSet.closure(set));
        implicationSet.closure(null);
    }

    @Test (expected = NullPointerException.class)
    public void testImplicationSet_isclosed() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        ImplicationSet implicationSet = new ImplicationSet(abstractContext);
        Set<String> set = new HashSet<>();
        assertTrue(implicationSet.isClosed(set));
        implicationSet.isClosed(null);
    }

    @Test (expected = NullPointerException.class)
    public void testImplicationSet_nextClosure() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        ImplicationSet implicationSet = new ImplicationSet(abstractContext);
        Set<String> set1 = new HashSet<>();
        Set<String> set2 = new HashSet<>();
        set1.add("1");
        set2.add("1");
        set2.add("2");
        assertFalse(implicationSet.nextClosure(set1) == implicationSet.nextClosure(set2));
        implicationSet.nextClosure(null);
    }

    @Test
    public void testPartialContext_getObjectAtIndex() {
        PartialContext partialContext = new PartialContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        partialContext.addObject(partialObject);
        assertTrue(partialContext.getObjectAtIndex(0) == partialObject);
    }

    @Test
    public void testPartialContext_removeObject1() {
        PartialContext partialContext = new PartialContext();
        try {
            partialContext.removeObject(null);
        }
        catch (Exception IllegalObjectException){
        }
    }

    @Test
    public void testPartialContext_removeObject2() {
        PartialContext partialContext = new PartialContext();
        try {
            partialContext.removeObject(null);
        }
        catch (Exception IllegalObjectException){
        }
    }

    @Test
    public void testPartialContext_refutes() {
        PartialContext partialContext = new PartialContext();
        assertFalse(partialContext.refutes(null));
    }

    @Test
    public void testPartialContext_addObject() {
        PartialContext partialContext = new PartialContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        assertTrue(partialContext.addObject(partialObject));
        assertFalse(partialContext.addObject(partialObject));
    }

    @Test
    public void testPartialContext_addAttributeToObject() {
        PartialContext partialContext = new PartialContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        partialContext.addObject(partialObject);
        try {
            partialContext.addAttributeToObject("1", partialObject.getIdentifier());
        }
        catch (Exception IllegalObjectException){
        }
    }

    @Test
    public void testPartialContext_removeAttributeFromObject() {
        PartialContext partialContext = new PartialContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        partialContext.addObject(partialObject);
        try {
            partialContext.removeAttributeFromObject("1", partialObject.getIdentifier());
        }
        catch (Exception IllegalObjectException){
        }
    }

    @Test (expected = NullPointerException.class)
    public void testPartialObject_refutes() {
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        partialObject.refutes(null);
    }


    @Test
    public void testPartialObjectDescription_addAttribute() {
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        assertTrue(partialObjectDescription.addAttribute(partialObject));
        assertFalse(partialObjectDescription.addAttribute(partialObject));
    }

    @Test
    public void testPartialObjectDescription_addNegateAttribute() {
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        assertTrue(partialObjectDescription.addNegatedAttribute(partialObject));
        assertFalse(partialObjectDescription.addNegatedAttribute(partialObject));
    }

    @Test
    public void testPartialObjectDescription_containsNegatedAttributes() {
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        Set<String> set = new HashSet<>();
        partialObjectDescription.addNegatedAttribute(set);
        assertTrue(partialObjectDescription.containsNegatedAttribute(set));
        set.add("1");
        assertFalse(partialObjectDescription.containsNegatedAttribute(set));
    }

    @Test (expected = NullPointerException.class)
    public  void mtestChangeAttributeOrderAction_actionPerformed(){
        ChangeAttributeOrderAction changeAttributeOrderAction = new ChangeAttributeOrderAction();
        changeAttributeOrderAction.actionPerformed(null);
    }

    @Test (expected = NullPointerException.class)
    public  void mtestCounterExampleProvidedAction_actionPerformed(){
        FCAObject fcaObject = new FCAObject() {
            @Override
            public Object getIdentifier() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public ObjectDescription getDescription() {
                return null;
            }

            @Override
            public boolean respects(FCAImplication implication) {
                return false;
            }
        };
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        CounterExampleProvidedAction counterExampleProvidedAction = new CounterExampleProvidedAction(abstractContext,fcaImplication,fcaObject);
        counterExampleProvidedAction.putValue("A1", new Object());
        counterExampleProvidedAction.setContext(abstractContext);
        counterExampleProvidedAction.actionPerformed(new ActionEvent(new Object(), 0, "A1"));
        counterExampleProvidedAction.actionPerformed(null);
    }

    @Test (expected = NullPointerException.class)
    public  void mtestQuestionConfirmedAction_actionPerformed(){
        QuestionConfirmedAction questionConfirmedAction = new QuestionConfirmedAction();
        questionConfirmedAction.actionPerformed(null);
    }

    @Test (expected = NullPointerException.class)
    public  void mtestQuestionRejectedAction_actionPerformed(){
        QuestionRejectedAction questionRejectedAction = new QuestionRejectedAction();
        questionRejectedAction.actionPerformed(null);
    }

    @Test
    public  void mtestQResetExplorationAction_actionPerformed(){
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        ResetExplorationAction<String, String, FCAObject<String, String>> resetExplorationAction = Mockito.mock(ResetExplorationAction.class);
        resetExplorationAction.putValue("A1", new Object());
        resetExplorationAction.setContext(abstractContext);
        resetExplorationAction.actionPerformed(new ActionEvent(new Object(), 0, "A1"));
        resetExplorationAction.actionPerformed(null);
    }

    @Test (expected = NullPointerException.class)
    public  void mtestStartExplorationAction_actionPerformed(){
        StartExplorationAction startExplorationAction = new StartExplorationAction();
        startExplorationAction.actionPerformed(null);
    }

    @Test
    public void mtestNewImplicationChange(){
        NewImplicationChange newImplicationChange = new NewImplicationChange(null,null);
        assertNull(newImplicationChange.getImplication());
        assertTrue(newImplicationChange.getType() == 1);
    }

    @Test (expected = NullPointerException.class)
    public void mtestNewObjectChange(){
        NewObjectChange newObjectChange = new NewObjectChange(null,null);
        assertNull(newObjectChange.getObject());
        assertTrue(newObjectChange.getType() == 2);
        newObjectChange.undo();
    }

    @Test
    public void mtestObjectHasAttributeChange(){
        ObjectHasAttributeChange objectHasAttributeChange = new ObjectHasAttributeChange(null,null);
        assertNull(objectHasAttributeChange.getObject());
        assertNull(objectHasAttributeChange.getAttribute());
        assertTrue(objectHasAttributeChange.getType() == 0);
    }

    @Test (expected = NullPointerException.class)
    public void mtestLisetSet_addAll(){
        ListSet listSet = new ListSet();
        listSet.addAll(null);
    }

    @Test (expected = NullPointerException.class)
    public void mtestLisetSet_removeAll(){
        ListSet listSet = new ListSet();
        listSet.removeAll(null);
    }

    @Test (expected = NullPointerException.class)
    public void mtestLisetSet_retainAll(){
        ListSet listSet = new ListSet();
        assertFalse(listSet.retainAll(null));
        Object object = new Object();
        listSet.add(object);
        listSet.retainAll(null);
    }

    @Test (expected = NullPointerException.class)
    public void mtestLisetSet_toArray(){
        ListSet listSet = new ListSet();
        listSet.toArray(null);
    }

    @Test //(expected = NullPointerException.class)
    public void mtestLisetSet_getIndexOf(){
        ListSet listSet = new ListSet();
        assertTrue(listSet.getIndexOf(null) == -1);
        listSet.add("1");
        listSet.add("2");
        listSet.add("3");
        assertTrue(listSet.getIndexOf("2") == 1);
    }
}
