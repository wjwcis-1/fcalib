package de.tudresden.inf.tcs.fcalib.test;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import de.tudresden.inf.tcs.fcaapi.*;
import de.tudresden.inf.tcs.fcaapi.action.ExpertAction;
import de.tudresden.inf.tcs.fcaapi.action.ExpertActionListener;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalContextException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalExpertException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcaapi.utils.IndexedSet;
import de.tudresden.inf.tcs.fcalib.*;
import de.tudresden.inf.tcs.fcalib.action.*;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;
import de.tudresden.inf.tcs.fcalib.change.ObjectHasAttributeChange;
import de.tudresden.inf.tcs.fcalib.utils.ListSet;
import org.apache.log4j.BasicConfigurator;
import org.junit.Test;
import org.mockito.Mockito;
import javax.management.Attribute;
import java.awt.event.ActionEvent;
import java.util.*;
import de.tudresden.inf.tcs.fcaapi.utils.IndexedSet;
import de.tudresden.inf.tcs.fcalib.AbstractContext;
import de.tudresden.inf.tcs.fcalib.action.CounterExampleProvidedAction;
import de.tudresden.inf.tcs.fcalib.utils.ListSet;
import org.junit.Test;
import org.mockito.internal.matchers.Null;

import java.util.List;

import static org.junit.Assert.*;

public class InputSpacePartition {
    @Test(expected = NullPointerException.class)
    public void testListSetCheck_add() {
        ListSet<Byte> set = new ListSet<>();
        Byte a = 1;
        assertTrue(set.add(a));
        assertNull(set.add(null));
    }

    @Test(expected = NullPointerException.class)
    public void testListSetCheck_addAll() {
        ListSet<Byte> set = new ListSet<>();
        assertFalse(set.addAll(set));
        assertNull(set.add(null));
    }

    @Test(expected = NullPointerException.class)
    public void testListSetCheck_Contains() {
        ListSet<Byte> set = new ListSet<>();
        Byte a = 1;
        set.add(a);
        assertTrue(set.contains(a));
        assertFalse(set.contains(null));
    }

    @Test(expected = NullPointerException.class)
    public void testListSetCheck_ContainsAll() {
        ListSet<Byte> set = new ListSet<>();
        assertTrue(set.containsAll(set));
        assertNull(set.containsAll(null));
    }

    @Test(expected = NullPointerException.class)
    public void testListSetCheck_equals() {
        ListSet<Byte> set = new ListSet<>();
        Byte a = 1;
        assertTrue(set.equals(set));
        assertFalse(set.equals(a));
        assertNull(set.equals(null));
    }

    @Test
    public void testListSetCheck_isEmpty() {
        ListSet<Byte> set = new ListSet<>();
        Byte a = 1;
        set.add(a);
        assertFalse(set.isEmpty());
    }

    @Test(expected = NullPointerException.class)
    public void testListSetCheck_Remove() {
        ListSet<Byte> set = new ListSet<>();
        Byte a = 1;
        set.add(a);
        assertTrue(set.remove(a));
        assertFalse(set.remove(null));
    }

    @Test(expected = NullPointerException.class)
    public void testListSetCheck_RemoveAll() {
        ListSet<Byte> set = new ListSet<>();
        Byte a = 1;
        set.add(a);
        assertTrue(set.removeAll(set));
        assertFalse(set.removeAll(null));
    }

    @Test(expected = NullPointerException.class)
    public void testListSetCheck_RetainAll() {
        ListSet<Byte> set = new ListSet<>();
        Byte a = 1;
        set.add(a);
        assertFalse(set.retainAll(set));
        assertNull(set.retainAll(null));
    }

    @Test
    public void testListSetCheck_size() {
        ListSet<Byte> set1 = new ListSet<>();
        ListSet<Byte> set2 = new ListSet<>();
        Byte a = 1;
        set1.add(a);
        assertTrue(set1.size() == set2.size()+1);
    }

    @Test
    public void testListSetCheck_getIndexOf() {
        ListSet<Byte> set = new ListSet<>();
        Byte a = 1;
        set.add(a);
        int n = set.size();
        assertTrue(set.getIndexOf(a) == n-1);
        assertTrue(set.getIndexOf(null) == -1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testListSetCheck_getElementAt() {
        ListSet<Byte> set = new ListSet<>();
        Byte a = 1;
        set.add(a);
        int n = set.size();
        assertTrue(set.getElementAt(a) == n-1);
        assertNull(set.getElementAt(n));
    }

    @Test
    public void testListSetCheck_toString() {
        ListSet<Byte> set = new ListSet<>();
        Byte a = 1;
        set.add(a);
        assertFalse(set.toString() == "{ }");
    }

    @Test (expected = NullPointerException.class)
    public void testChangeAttributeOrderAction_actionPerformed() {
        ChangeAttributeOrderAction set = new ChangeAttributeOrderAction();
        set.actionPerformed(null);
    }

    @Test (expected = NullPointerException.class)
    public void testCounterExampleProvidedAction_actionPerformed() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        FCAImplication implication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FCAObject object = new FCAObject() {
            @Override
            public Object getIdentifier() {
                return new Object();
            }

            @Override
            public String getName() {
                return "1223";
            }

            @Override
            public ObjectDescription getDescription() {
                return new ObjectDescription() {
                    @Override
                    public boolean containsAttribute(Object attribute) {
                        return false;
                    }

                    @Override
                    public boolean containsAttributes(Set attrs) {
                        return false;
                    }

                    @Override
                    public boolean addAttribute(Object attribute) {
                        return false;
                    }

                    @Override
                    public boolean addAttributes(Set attrs) {
                        return false;
                    }

                    @Override
                    public boolean removeAttribute(Object attribute) {
                        return false;
                    }

                    @Override
                    public Object clone() {
                        return null;
                    }
                };
            }

            @Override
            public boolean respects(FCAImplication implication) {
                return true;
            }
        };
        CounterExampleProvidedAction set = new CounterExampleProvidedAction(abstractContext,implication,object);
        set.actionPerformed(null);
    }

    @Test (expected = NullPointerException.class)
    public void testQuestionConfirmedAction_actionPerformed() {
        QuestionConfirmedAction set = new QuestionConfirmedAction();
        set.actionPerformed(null);
    }

    @Test (expected = NullPointerException.class)
    public void testQuestionRejectedAction_actionPerformed() {
        QuestionRejectedAction set = new QuestionRejectedAction();
        set.actionPerformed(null);
    }

    @Test //(expected = NullPointerException.class)
    public void testResetExplorationAction_actionPerformed() {
        ResetExplorationAction<String,String,FCAObject<String,String>> set = Mockito.mock(ResetExplorationAction.class);
        set.actionPerformed(null);
    }

    @Test (expected = NullPointerException.class)
    public void testStartExplorationAction_actionPerformed() {
        StartExplorationAction set = new StartExplorationAction();
        set.actionPerformed(null);
    }

    @Test
    public void testStopExplorationAction_actionPerformed() {
        StopExplorationAction set = new StopExplorationAction();
        Object o = new Object();
        int i = 1;
        String command = "Stop now";
        ActionEvent actionEvent = new ActionEvent(o,i,command);
        set.actionPerformed(actionEvent);
        assertTrue(true);
    }

    @Test (expected = NullPointerException.class)
    public void NewImplicationChange_undo() {
        NewImplicationChange set = new NewImplicationChange(null,null);
        set.undo();
    }

    @Test //(expected = NullPointerException.class)
    public void NewImplicationChange_getType() {
        Context context = new Context() {
            @Override
            public boolean addAttribute(Object attr) throws IllegalAttributeException {
                return false;
            }

            @Override
            public boolean addAttributes(Set attrs) throws IllegalAttributeException {
                return false;
            }

            @Override
            public IndexedSet getAttributes() {
                return null;
            }

            @Override
            public int getAttributeCount() {
                return 0;
            }

            @Override
            public Object getAttributeAtIndex(int index) {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public int getObjectCount() {
                return 0;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addObjects(Set objects) throws IllegalObjectException {
                return false;
            }

            @Override
            public void clearObjects() {

            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public boolean containsObject(Object id) {
                return false;
            }

            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set doublePrime(Set attributes) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public boolean isExpertSet() {
                return false;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }

            @Override
            public Set<FCAImplication> getImplications() {
                return null;
            }

            @Override
            public void expertPerformedAction(ExpertAction action) {

            }
        };
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        NewImplicationChange set1 = new NewImplicationChange(context,fcaImplication);
        NewImplicationChange set2 = new NewImplicationChange(null,null);
        assertTrue(set1.getType() == 1);
        assertTrue(set2.getType() == 1);
    }

    @Test (expected = NullPointerException.class)
    public void NewObjectChange_undo() {
        Context context = new Context() {
            @Override
            public boolean addAttribute(Object attr) throws IllegalAttributeException {
                return false;
            }

            @Override
            public boolean addAttributes(Set attrs) throws IllegalAttributeException {
                return false;
            }

            @Override
            public IndexedSet getAttributes() {
                return null;
            }

            @Override
            public int getAttributeCount() {
                return 0;
            }

            @Override
            public Object getAttributeAtIndex(int index) {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public int getObjectCount() {
                return 0;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addObjects(Set objects) throws IllegalObjectException {
                return false;
            }

            @Override
            public void clearObjects() {

            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public boolean containsObject(Object id) {
                return false;
            }

            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set doublePrime(Set attributes) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public boolean isExpertSet() {
                return false;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }

            @Override
            public Set<FCAImplication> getImplications() {
                return null;
            }

            @Override
            public void expertPerformedAction(ExpertAction action) {

            }
        };
        FCAObject object = new FCAObject() {
            @Override
            public Object getIdentifier() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public ObjectDescription getDescription() {
                return null;
            }

            @Override
            public boolean respects(FCAImplication implication) {
                return false;
            }
        };
        NewObjectChange set1 = new NewObjectChange(context,object);
        NewObjectChange set2 = new NewObjectChange(null,null);
        set1.undo();
        set2.undo();
    }

    @Test
    public void NewObjectChange_getType() {
        Context context = new Context() {
            @Override
            public boolean addAttribute(Object attr) throws IllegalAttributeException {
                return false;
            }

            @Override
            public boolean addAttributes(Set attrs) throws IllegalAttributeException {
                return false;
            }

            @Override
            public IndexedSet getAttributes() {
                return null;
            }

            @Override
            public int getAttributeCount() {
                return 0;
            }

            @Override
            public Object getAttributeAtIndex(int index) {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public int getObjectCount() {
                return 0;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addObjects(Set objects) throws IllegalObjectException {
                return false;
            }

            @Override
            public void clearObjects() {

            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public boolean containsObject(Object id) {
                return false;
            }

            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set doublePrime(Set attributes) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public boolean isExpertSet() {
                return false;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }

            @Override
            public Set<FCAImplication> getImplications() {
                return null;
            }

            @Override
            public void expertPerformedAction(ExpertAction action) {

            }
        };
        FCAObject object = new FCAObject() {
            @Override
            public Object getIdentifier() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public ObjectDescription getDescription() {
                return null;
            }

            @Override
            public boolean respects(FCAImplication implication) {
                return false;
            }
        };
        NewObjectChange set1 = new NewObjectChange(context,object);
        NewObjectChange set2 = new NewObjectChange(null,null);
        assertTrue(set1.getType() == 2);
        assertTrue(set2.getType() == 2);
    }

    @Test (expected = NullPointerException.class)
    public void ObjectHasAttributeChange_undo() {
        FCAObject object = new FCAObject() {
            @Override
            public Object getIdentifier() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public ObjectDescription getDescription() {
                return null;
            }

            @Override
            public boolean respects(FCAImplication implication) {
                return false;
            }
        };
        ObjectHasAttributeChange set1 = new ObjectHasAttributeChange(object,new Object());
        ObjectHasAttributeChange set2 = new ObjectHasAttributeChange(null,null);
        set1.undo();
        set2.undo();
    }

    @Test
    public void ObjectHasAttributeChange_getType() {
        FCAObject object = new FCAObject() {
            @Override
            public Object getIdentifier() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public ObjectDescription getDescription() {
                return null;
            }

            @Override
            public boolean respects(FCAImplication implication) {
                return false;
            }
        };
        ObjectHasAttributeChange set1 = new ObjectHasAttributeChange(object,new Object());
        ObjectHasAttributeChange set2 = new ObjectHasAttributeChange(null,null);
        assertTrue(set1.getType() == 0);
        assertTrue(set2.getType() == 0);
    }

    private Set<FCAObject<String,String>> absset = new Set<FCAObject<String, String>>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }
        @Override
        public Iterator<FCAObject<String, String>> iterator() {
            return null;
        }
        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray( T[] a) {
            return null;
        }

        @Override
        public boolean add(FCAObject<String, String> stringStringFCAObject) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll( Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll( Collection<? extends FCAObject<String, String>> c) {
            return false;
        }

        @Override
        public boolean retainAll( Collection<?> c) {
            return false;
        }

        @Override
        public boolean removeAll( Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }
    };

    @Test (expected = NullPointerException.class)
    public void ListSetTest_add() {
        // test boolean add function in ListSet.java, Due to the object of add function can't be null
        // (it already throw when doing this process), so we just test the element already added in or not?

        ListSet<String> test2 = new ListSet<>();
        test2.add(null);
        assertNull(test2);

        test2.add("1");
        assertFalse(test2.add("1"));
        assertTrue(test2.add("2"));
        assertNotNull(test2);
    }

    @Test(expected = NullPointerException.class)
    public void ListSetTest_addAll() {
        ListSet<Byte> test2 = new ListSet<>();
        assertFalse(test2.addAll(test2));
        test2.addAll(null);
    }

    @Test(expected = NullPointerException.class)
    public void ListSetTest_contains() {
        ListSet<String> test2 = new ListSet<>();
        Collection<String> collection = new HashSet<>();

        test2.contains("");
        test2.contains(null);

    }

    @Test (expected = IllegalAttributeException.class)

    public void testAbstractContext() throws IllegalObjectException {
        AbstractContext<String,String,FCAObject<String,String>> abstractContext = new AbstractContext<String, String, FCAObject<String, String>>() {
            @Override
            public IndexedSet<FCAObject<String, String>> getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(String id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject<String, String> object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(String attribute, String id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set<String> doublePrime(Set<String> x) {
                return null;
            }

            @Override
            public Set<FCAImplication<String>> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication<String> imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject<String, String> counterExample, FCAImplication<String> imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication<String> implication) {
                return false;
            }

            @Override
            public FCAObject<String, String> getObject(String id) {
                return null;
            }

            @Override
            public FCAObject<String, String> getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject<String, String> object, String attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication<String>> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert<String, String, FCAObject<String, String>> getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert<String, String, FCAObject<String, String>> e) {

            }
        };
        abstractContext.addAttribute("1");
        abstractContext.addAttribute("2");
        Set<String> att = new HashSet<>();
        att.add("1");
        att.add("4");

        // test addAttributes
        assertNotNull(abstractContext.addAttributes(att));

        // test removeObject
        assertTrue(abstractContext.removeObject("1"));

        // test addAttributeToObject
        assertTrue(abstractContext.addAttributeToObject("2", "1"));

        // test double Prime
        assertNotNull(abstractContext.doublePrime(att));



        // test isCounterExampleValid
        FCAImplication<String> implication = new FCAImplication<String>() {
            @Override
            public Set<String> getPremise() {
                return null;
            }

            @Override
            public Set<String> getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication<String> imp) {
                return false;
            }
        };
        FCAObject<String, String> attr = new FCAObject<String, String>() {
            @Override
            public String getIdentifier() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public ObjectDescription<String> getDescription() {
                return null;
            }

            @Override
            public boolean respects(FCAImplication<String> implication) {
                return false;
            }
        };
        // test refutes
        assertTrue(abstractContext.refutes(implication));

        //test isCounterExampleValid
        assertTrue(abstractContext.isCounterExampleValid(attr,implication));

        // test addObject
        assertFalse(abstractContext.addObject(attr));

        // test getObject
        assertNull(abstractContext.getObject("1"));

        // test getObjectAtIndex
        assertNull(abstractContext.getObjectAtIndex(1));

        // test objectHasAttribute
        String abb = new String("123");
        assertTrue(abstractContext.objectHasAttribute(attr,abb));

        // test containsObject
        assertTrue(abstractContext.containsObject("1"));

        // test addObjects
        assertTrue(abstractContext.addObjects(absset));

        // test getImplications
        abstractContext.getObjectAtIndex(1);

        // test getNextPremise
        abstractContext.getNextPremise(att);


    }

    @Test
    public void testAbstractExpert(){
        AbstractExpert<String,String,FCAObject<String,String>> abstractExpert1 = new AbstractExpert<String, String, FCAObject<String, String>>() {
            @Override
            public void requestCounterExample(FCAImplication<String> question) {

            }

            @Override
            public void askQuestion(FCAImplication<String> question) {

            }

            @Override
            public void counterExampleInvalid(FCAObject<String, String> counterExample, int reason) {

            }

            @Override
            public void forceToCounterExample(FCAImplication<String> implication) {

            }

            @Override
            public void explorationFinished() {

            }

            @Override
            public void implicationFollowsFromBackgroundKnowledge(FCAImplication<String> implication) {

            }
        };
        ExpertActionListener<String,String> listener = new ExpertActionListener<String, String>() {
            @Override
            public void expertPerformedAction(ExpertAction action) {

            }
        };
        abstractExpert1.addExpertActionListener(listener);
        abstractExpert1.removeExpertActionListeners();
    }
    @Test (expected = IndexOutOfBoundsException.class)
    public void testFormalContext_getObjectAtIndex() {
        FormalContext<String, String> context = new FormalContext<>();
        FullObject object = new FullObject(context);
        try{
            context.addObject(null);
        }catch (Exception IllegalObjectException){
        }

        // test getObjectAtIndex
        context.getObjectAtIndex(1);

    }

    @Test
    public void testFormalContext_getObject() {
        FormalContext<String, String> context = new FormalContext<>();
        // test getObject
        assertNull(context.getObject(null));
    }

    @Test //(expected = IllegalObjectException.class)
    public void testFormalContext_removeObject() {
        FormalContext<String, String> context = new FormalContext<>();
        try{
            context.removeObject("2");
        }catch (Exception IllegalObjectException){
        }

    }

    @Test //(expected = IllegalObjectException.class)
    public void testFormalContext_removeObject3() {
        FormalContext<String, String> context = new FormalContext<>();
        FullObject object = new FullObject(context);
        try{
            context.removeObject(object);
        }catch (Exception IllegalObjectException){
        }
    }

    @Test //(expected = IllegalAttributeException.class)
    public void testFormalContext_addAttributeToObject() {
        FormalContext<String, String> context = new FormalContext<>();
        FullObject object = new FullObject(context);
        context.addAttribute("2");
        try{
            context.addAttributeToObject(null,null);
        }catch (Exception IllegalObjectException){
        }

    }
    @Test //(expected = IllegalAttributeException.class)
    public void testFormalContext_removeAttributeFromObject() {
        FormalContext<String, String> context = new FormalContext<>();
        FullObject object = new FullObject(context);
        context.addAttribute("2");
        try{
            context.removeAttributeFromObject(null, null);
        }catch (Exception IllegalObjectException){
        }

    }
    @Test //(expected = IllegalAttributeException.class)
    public void testFormalContext_objectHasAttribute() {
        FormalContext<String, String> context = new FormalContext<>();
        FullObject object = new FullObject(context);
        assertFalse(context.objectHasAttribute(object,"2"));

    }

    @Test
    public void testFormalContext_doublePrime(){
        FormalContext<String, String> context = new FormalContext<>();
        //FullObject object = new FullObject(context);
        Set<String> temp = new HashSet<>();
        context.doublePrime(temp);
    }

    @Test
    public void testFormalContext_closure(){
        FormalContext<String, String> context = new FormalContext<>();
        //FullObject object = new FullObject(context);
        Set<String> temp = new HashSet<>();
        context.closure(temp);
    }
    @Test
    public void testFormalContext_isClosed(){
        FormalContext<String, String> context = new FormalContext<>();
        //FullObject object = new FullObject(context);
        Set<String> temp = new HashSet<>();
        context.isClosed(temp);
    }

    @Test
    public void testFormalContext_refutes(){
        FormalContext<String, String> context = new FormalContext<>();
        //FullObject object = new FullObject(context);
        FCAImplication impl = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        Set<String> temp = new HashSet<>();
        context.refutes(impl);
    }

    @Test (expected = NullPointerException.class)
    public void testFormalContext_isCounterExampleValid(){
        FormalContext<String, String> context = new FormalContext<>();
        FullObject object = new FullObject(context);
        FCAImplication impl = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        context.isCounterExampleValid(object,impl);

    }

    @Test
    public void testFormalContext_followsFromBackgroundKnowledge(){
        FormalContext<String, String> context = new FormalContext<>();
        FullObject object = new FullObject(context);
        FCAImplication impl = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        context.followsFromBackgroundKnowledge(impl);
    }

    @Test
    public void testFormalContext_setExpert(){
        Expert<String, String, FullObject<String, String>> expert = new Expert<String, String, FullObject<String, String>>() {
            @Override
            public void askQuestion(FCAImplication<String> question) {

            }

            @Override
            public void requestCounterExample(FCAImplication<String> question) {

            }

            @Override
            public void addExpertActionListener(ExpertActionListener<String, String> listener) {

            }

            @Override
            public void removeExpertActionListeners() {

            }

            @Override
            public void counterExampleInvalid(FullObject<String, String> counterExample, int reasonCode) {

            }

            @Override
            public void explorationFinished() {

            }

            @Override
            public void forceToCounterExample(FCAImplication<String> implication) {

            }

            @Override
            public void implicationFollowsFromBackgroundKnowledge(FCAImplication<String> implication) {

            }
        };
        FormalContext<String, String> context = new FormalContext<>();
//        FullObject object = new FullObject(context);

        context.setExpert((Expert<String, String, FullObject<String, String>>) expert);
    }

    @Test
    public void testFullObject_respects(){
        FullObject<String,String> fullObject1 = Mockito.mock(FullObject.class);
        FCAImplication imp = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        assertFalse(fullObject1.respects(imp));
        assertFalse(fullObject1.respects(null));

    }

    @Test
    public void testFullObject_refutes(){
        FullObject<String,String> fullObject1 = Mockito.mock(FullObject.class);
        FCAImplication imp = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        assertFalse(fullObject1.refutes(imp));
        assertFalse(fullObject1.refutes(null));

    }

    @Test
    public void testFullObjectDescription_containsAttribute(){
        FullObjectDescription fullObjectDescription1 = new FullObjectDescription();
        //Set<Attribute> set = new HashSet<>();
        Object object = new Object();
        fullObjectDescription1.addAttribute(new Attribute("1", object));
        //set.add(new Attribute("1", new Object()));
        assertTrue(fullObjectDescription1.containsAttribute(new Attribute("1", object)));
    }

    @Test
    public void testFullObjectDescription_containsAttributes(){
        FullObjectDescription fullObjectDescription1 = new FullObjectDescription();
        Set<Attribute> set = new HashSet<>();
        Object object = new Object();
        set.add(new Attribute("1", object));

        fullObjectDescription1.addAttributes(set);
        //set.add(new Attribute("1", new Object()));
        assertTrue(fullObjectDescription1.containsAttributes(set));
    }

    @Test
    public void testFullObjectDescription_addAttribute(){
        FullObjectDescription fullObjectDescription1 = new FullObjectDescription();
        Object object = new Object();
        assertTrue(fullObjectDescription1.addAttribute(null));
        assertTrue(fullObjectDescription1.addAttribute(new Attribute("1", object)));
    }

    @Test (expected = NullPointerException.class)
    public void testFullObjectDescription_addAttributes(){
        FullObjectDescription fullObjectDescription1 = new FullObjectDescription();
        Set<Attribute> set = new HashSet<>();
        Object object = new Object();
        set.add(new Attribute("1", object));

        assertTrue(fullObjectDescription1.addAttributes(set));
        //set.add(new Attribute("1", new Object()));
        fullObjectDescription1.addAttributes(null);
    }

    @Test
    public void testFullObjectDescription_removeAttribute(){
        FullObjectDescription fullObjectDescription1 = new FullObjectDescription();
        Object object = new Object();
        assertTrue(fullObjectDescription1.addAttribute(null));
        assertTrue(fullObjectDescription1.addAttribute(new Attribute("1", object)));
        assertTrue(fullObjectDescription1.removeAttribute(new Attribute("1", object)));
        assertFalse(fullObjectDescription1.removeAttribute(new Attribute("0",object)));
    }

    @Test
    public void testFullObjectDescription_clone(){
        FullObjectDescription fullObjectDescription1 = new FullObjectDescription();
        try{
            fullObjectDescription1.clone();
        } catch (Exception CloneNotSupportedException) {
        };
    }

    @Test
    public void testImplication_equals(){
        Implication implication = new Implication();
        FCAImplication implication1 = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        assertFalse(implication.equals(implication1));
        assertTrue(implication.equals(implication));
    }

    @Test (expected = NullPointerException.class)
    public void testImplicationSet_add(){
        ImplicationSet implicationSet = new ImplicationSet(new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        });
        FCAImplication implication1 = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }
            @Override
            public Set getConclusion() {
                return null;
            }
            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        assertTrue(implicationSet.add(implication1));
        assertFalse(implicationSet.add(null));
    }

    @Test (expected = NullPointerException.class)
    public void testImplicationSet_closure(){
        ImplicationSet implicationSet = new ImplicationSet(new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        });
        Set<String> set = new HashSet<>();
        ImplicationSet<String> implicationSet1 = Mockito.mock(ImplicationSet.class);
        set.add("a");
        set.add("b");
        implicationSet1.closure(set);
        implicationSet.closure(null);
    }

    @Test
    public void testImplicationSet_isClosed(){
        ImplicationSet implicationSet = new ImplicationSet(new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        });
        Set<String> set = new HashSet<>();
        implicationSet.closure(set);
        assertTrue(implicationSet.isClosed(set));
    }
    @Test (expected = NullPointerException.class)
    public void testImplicationSet_nextClosure(){
        ImplicationSet implicationSet = new ImplicationSet(new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        });
        Set<String> set = new HashSet<>();
        implicationSet.nextClosure(set);
        implicationSet.nextClosure(null);
    }
    @Test
    public void testPartialContext_getObject(){
        PartialContext partialContext = new PartialContext();
        assertNull(partialContext.getObject(null));
    }
    @Test
    public void testPartialContext_getObjectAtIndex(){
        //PartialContext<String, String, PartialObject<String, String>> partialContext = new PartialContext<>();
        PartialContext partialContext = new PartialContext();
        PartialObject<String,String> object1 = Mockito.mock(PartialObject.class);
        partialContext.addObject(object1);
        assertEquals(partialContext.getObjectAtIndex(partialContext.getObjectCount()-1),object1);
    }

    @Test
    public void testPartialContext_removeObject(){
        PartialContext<String, String, PartialObject<String, String>> partialContext = new PartialContext<>();
        partialContext.addAttribute("A1");
        PartialObject<String,String> object1 = new PartialObject<>("1");
        object1.setName("a1");
        partialContext.addObject(object1);

        String id = object1.getIdentifier();
        partialContext.getObject(id);
        //partialContext.removeObject(id);
        try{
            partialContext.removeObject("0");
        } catch (Exception IllegalObjectException) {
        }
    }

    @Test
    public void testPartialContext_refutes(){
        PartialContext partialContext = new PartialContext();
        assertFalse(partialContext.refutes(null));
    }


    @Test (expected = NullPointerException.class)
    public void testPartialContext_isCounterExampleValid(){
        PartialContext partialContext = new PartialContext();
        assertFalse(partialContext.isCounterExampleValid(null,null));
    }

    @Test (expected = NullPointerException.class)
    public void testPartialContext_addObject(){
        PartialContext partialContext = new PartialContext();
        partialContext.addObject(null);
    }

    @Test
    public void testPartialContext_clearObjects(){
        PartialContext partialContext = new PartialContext();
        partialContext.clearObjects();
    }


    @Test
    public void testPartialContext_addAttributeToObject(){
        PartialContext partialContext = new PartialContext();
        partialContext.addAttribute("2");
        try{
            partialContext.addAttributeToObject(null,null);
        }catch (Exception IllegalObjectException){
        }
    }

    @Test
    public void testPartialContext_removeAttributeFromObject(){
        PartialContext partialContext = new PartialContext();
        partialContext.addAttribute("2");
        try{
            partialContext.removeAttributeFromObject(null, null);
        }catch (Exception IllegalObjectException){
        }
    }

    @Test (expected = NullPointerException.class)
    public void testPartialContext_objectHasAttribute(){
        PartialContext partialContext = new PartialContext();
        PartialObject object = new PartialObject(partialContext);
        assertFalse(partialContext.objectHasAttribute(object,"2"));
        String abb = new String("123");
        partialContext.addAttribute(null);
        assertTrue(partialContext.objectHasAttribute(object,null));
    }

    @Test (expected = NullPointerException.class)
    public void testPartialContext_objectHasNegatedAttribute(){
        PartialContext partialContext = new PartialContext();
        PartialObject object = new PartialObject(partialContext);
        assertFalse(partialContext.objectHasNegatedAttribute(object,"2"));
        String abb = new String("123");
        partialContext.addAttribute(null);
        assertTrue(partialContext.objectHasNegatedAttribute(object,null));
    }

    @Test
    public void testPartialContext_doublePrime(){
        PartialContext partialContext = new PartialContext();
        Set<String> temp = new HashSet<>();
        partialContext.doublePrime(temp);
    }


    @Test (expected = NullPointerException.class)
    public void testPartialObject_respects(){
        Set<String> strings = new HashSet<>();
        strings.add("1");
        strings.add("2");
        PartialObject partialObject =  new PartialObject("123", strings);

        FCAImplication imp = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        assertFalse(partialObject.respects(imp));
        assertFalse(partialObject.respects(null));
    }

    @Test (expected = NullPointerException.class)
    public void testPartialObject_refutes(){
        Set<String> strings = new HashSet<>();
        strings.add("1");
        strings.add("2");
        PartialObject partialObject =  new PartialObject("123", strings);

        FCAImplication imp = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        assertFalse(partialObject.refutes(null));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testPartialObjectDescription_addAttribute(){
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        Object object = new Object();
        assertTrue(partialObjectDescription.addAttribute(new Attribute("1",object)));
        partialObjectDescription.addNegatedAttribute(new Attribute("2",object));
        partialObjectDescription.addAttribute(new Attribute("2",object));
    }

    @Test (expected = IllegalAttributeException.class)
    public void testPartialObjectDescription_addNegatedAttribute(){
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        Object object = new Object();
        assertTrue(partialObjectDescription.addNegatedAttribute(new Attribute("1",object)));
        partialObjectDescription.addAttribute(new Attribute("2",object));
        partialObjectDescription.addNegatedAttribute(new Attribute("2",object));
    }

    @Test
    public void testPartialObjectDescription_containsNegatedAttribute(){
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        Object object = new Object();
        partialObjectDescription.addNegatedAttribute(new Attribute("2",object));
        partialObjectDescription.containsNegatedAttribute(new Attribute("2",object));
    }

    @Test
    public void testPartialObjectDescription_containsNegatedAttributes(){
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        partialObjectDescription.addNegatedAttribute("1");
        assertTrue(partialObjectDescription.containsNegatedAttribute("1"));
        assertFalse(partialObjectDescription.containsNegatedAttribute(null));
    }


    /*
     mutation
      */
    @Test(expected = NullPointerException.class)
    public void testAbstractContext_getObjectCount() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        abstractContext.getObjectCount();
    }

    @Test(expected = IllegalAttributeException.class)
    public void testAbstractContext_addAttributes() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        Set<String> att = new HashSet<>();
        att.add("1");
        att.add("2");
        assertTrue(abstractContext.addAttributes(att));
        abstractContext.addAttributes(att);
    }

    @Test
    public void testAbstractContext_addObjects() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        Set<Object> object = new HashSet<>();
        try {
            abstractContext.addObjects(object);
        } catch (Exception IllegalObjcetException) {
        }
    }

    @Test(expected = NullPointerException.class)
    public void testAbstractContext_clearObjects() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        abstractContext.clearObjects();
    }

    @Test(expected = NullPointerException.class)
    public void testAbstractContext_getNextPremise() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        abstractContext.getNextPremise(null);
    }

    @Test(expected = NullPointerException.class)
    public void testAbstractContext_expertPerformedAction() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        abstractContext.expertPerformedAction(null);
    }

    @Test(expected = NullPointerException.class)
    public void testAbstractContext_continueExploration() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        Implication<String> implication = new Implication<>();
        implication.getPremise().add("a");
        implication.getPremise().add("b");
        abstractContext.continueExploration(implication.getPremise());
        abstractContext.continueExploration(null);
    }


    @Test //(expected = NullPointerException.class)
    public void testAbstractExpert_removeExpertActionListeners() {
        AbstractExpert abstractExpert = new AbstractExpert() {
            @Override
            public void requestCounterExample(FCAImplication question) {

            }

            @Override
            public void askQuestion(FCAImplication question) {

            }

            @Override
            public void counterExampleInvalid(FCAObject counterExample, int reason) {

            }

            @Override
            public void forceToCounterExample(FCAImplication implication) {

            }

            @Override
            public void explorationFinished() {

            }

            @Override
            public void implicationFollowsFromBackgroundKnowledge(FCAImplication implication) {

            }
        };
        abstractExpert.removeExpertActionListeners();
    }

    @Test //(expected = NullPointerException.class)
    public void testAbstractExpert_fireExpertAction() {
        AbstractExpert abstractExpert = new AbstractExpert() {
            @Override
            public void requestCounterExample(FCAImplication question) {

            }

            @Override
            public void askQuestion(FCAImplication question) {

            }

            @Override
            public void counterExampleInvalid(FCAObject counterExample, int reason) {

            }

            @Override
            public void forceToCounterExample(FCAImplication implication) {

            }

            @Override
            public void explorationFinished() {

            }

            @Override
            public void implicationFollowsFromBackgroundKnowledge(FCAImplication implication) {

            }
        };
        abstractExpert.fireExpertAction(null);
    }


    @Test
    public void testFormalContext_addObject() {
        FormalContext formalContext = new FormalContext();
        Set<Object> object = new HashSet<>();
        try {
            formalContext.addObjects(object);
        } catch (Exception IllegalObjcetException) {
        }
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void mtestFormalContext_getObjectAtIndex() {
        FormalContext formalContext = new FormalContext();
        formalContext.getObjectAtIndex(-1);
    }

    @Test
    public void mtestFormalContext_removeObject() {
        FormalContext formalContext = new FormalContext();
        try{
            formalContext.removeObject(null);
        }catch (Exception IllegalObjcetException) {
        }
    }

    @Test
    public void mtestFormalContext_addAttributeToObject() {
        FormalContext formalContext = new FormalContext();
        try{
            formalContext.addAttributeToObject("1",null);
        }catch (Exception IllegalObjcetException) {
        }
    }

    @Test
    public void mtestFormalContext_removeAttributeFromObject() {
        FormalContext formalContext = new FormalContext();
        try{
            formalContext.removeAttributeFromObject("1",null);
        }catch (Exception IllegalObjcetException) {
        }
    }

    @Test
    public void mtestFormalContext_objectHasAttribute() {
        FormalContext formalContext = new FormalContext();
        //FullObject<String, String> object1 = new FullObject<>("1");
        try{
            formalContext.objectHasAttribute(null,"1");
        }catch (Exception IllegalObjcetException) {
        }
    }

    @Test
    public void testFormalContext_objectHasAttribute2() {
        FormalContext formalContext = new FormalContext();
        FullObject<String, String> object = new FullObject<>("1");
        try{
            formalContext.objectHasAttribute(object,null);
        }catch (Exception IllegalObjcetException) {
        }
    }

    @Test
    public void mtestFormalContext_closure() {
        FormalContext formalContext = new FormalContext();
        Set<String> set = new HashSet<>();
        assertFalse(formalContext.closure(null) == formalContext.closure(set));
    }

    @Test (expected = NullPointerException.class)
    public void testFormalContext_isclosed() {
        FormalContext formalContext = new FormalContext();
        Set<String> set = new HashSet<>();
        assertTrue(formalContext.isClosed(set));
        formalContext.isClosed(null);
    }

    @Test
    public void mtestFormalContext_refutes() {
        FormalContext formalContext = new FormalContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        assertFalse(formalContext.refutes(fcaImplication));
        assertFalse(formalContext.refutes(null));
    }

    @Test
    public void mtestFormalContext_followsFromBackgroundKnowledge() {
        FormalContext formalContext = new FormalContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        assertFalse(formalContext.followsFromBackgroundKnowledge(null));
    }

    @Test (expected = NullPointerException.class)
    public void mtestFullObject_refutes() {
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FullObject fullObject = new FullObject(fcaImplication);
        fullObject.refutes(null);
    }

    @Test
    public void mtestFullObjectDescription_addAttribute() {
        FullObjectDescription fullObjectDescription = new FullObjectDescription();
        fullObjectDescription.addAttribute("1");
        assertFalse(fullObjectDescription.addAttribute("1"));
        assertTrue(fullObjectDescription.addAttribute(null));
    }

    @Test
    public void mtestFullObjectDescription_addAttributes() {
        FullObjectDescription fullObjectDescription = new FullObjectDescription();
        fullObjectDescription.addAttribute("1");
        fullObjectDescription.addAttribute("2");
        Set<String> set = new HashSet<>();
        set.add("12");
        assertTrue(fullObjectDescription.addAttributes(set));
    }

    @Test
    public void mtestFullObjectDescription_removeAttribute() {
        FullObjectDescription fullObjectDescription = new FullObjectDescription();
        fullObjectDescription.addAttribute("1");
        assertTrue(fullObjectDescription.removeAttribute("1"));
        assertFalse(fullObjectDescription.removeAttribute("2"));
    }

    @Test (expected = NullPointerException.class)
    public void mtestImplicationSet_add() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        ImplicationSet implicationSet = new ImplicationSet(abstractContext);
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        assertTrue(implicationSet.add(fcaImplication));
        assertFalse(implicationSet.add(fcaImplication));
        implicationSet.add(null);
    }

    @Test (expected = NullPointerException.class)
    public void mtestImplicationSet_closure() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        ImplicationSet implicationSet = new ImplicationSet(abstractContext);
        Set<String> set = new HashSet<>();
        assertFalse(implicationSet.closure(null) == implicationSet.closure(set));
        implicationSet.closure(null);
    }

    @Test (expected = NullPointerException.class)
    public void testImplicationSet_isclosed() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        ImplicationSet implicationSet = new ImplicationSet(abstractContext);
        Set<String> set = new HashSet<>();
        assertTrue(implicationSet.isClosed(set));
        implicationSet.isClosed(null);
    }

    @Test (expected = NullPointerException.class)
    public void mtestImplicationSet_nextClosure() {
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        ImplicationSet implicationSet = new ImplicationSet(abstractContext);
        Set<String> set1 = new HashSet<>();
        Set<String> set2 = new HashSet<>();
        set1.add("1");
        set2.add("1");
        set2.add("2");
        assertFalse(implicationSet.nextClosure(set1) == implicationSet.nextClosure(set2));
        implicationSet.nextClosure(null);
    }

    @Test
    public void mtestPartialContext_getObjectAtIndex() {
        PartialContext partialContext = new PartialContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        partialContext.addObject(partialObject);
        assertTrue(partialContext.getObjectAtIndex(0) == partialObject);
    }

    @Test
    public void testPartialContext_removeObject1() {
        PartialContext partialContext = new PartialContext();
        try {
            partialContext.removeObject(null);
        }
        catch (Exception IllegalObjectException){
        }
    }

    @Test
    public void testPartialContext_removeObject2() {
        PartialContext partialContext = new PartialContext();
        try {
            partialContext.removeObject(null);
        }
        catch (Exception IllegalObjectException){
        }
    }

    @Test
    public void mtestPartialContext_refutes() {
        PartialContext partialContext = new PartialContext();
        assertFalse(partialContext.refutes(null));
    }

    @Test
    public void mtestPartialContext_addObject() {
        PartialContext partialContext = new PartialContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        assertTrue(partialContext.addObject(partialObject));
        assertFalse(partialContext.addObject(partialObject));
    }

    @Test
    public void mtestPartialContext_addAttributeToObject() {
        PartialContext partialContext = new PartialContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        partialContext.addObject(partialObject);
        try {
            partialContext.addAttributeToObject("1", partialObject.getIdentifier());
        }
        catch (Exception IllegalObjectException){
        }
    }

    @Test
    public void mtestPartialContext_removeAttributeFromObject() {
        PartialContext partialContext = new PartialContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        partialContext.addObject(partialObject);
        try {
            partialContext.removeAttributeFromObject("1", partialObject.getIdentifier());
        }
        catch (Exception IllegalObjectException){
        }
    }

    @Test (expected = NullPointerException.class)
    public void mtestPartialObject_refutes() {
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        partialObject.refutes(null);
    }


    @Test
    public void mtestPartialObjectDescription_addAttribute() {
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        assertTrue(partialObjectDescription.addAttribute(partialObject));
        assertFalse(partialObjectDescription.addAttribute(partialObject));
    }

    @Test
    public void testPartialObjectDescription_addNegateAttribute() {
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        assertTrue(partialObjectDescription.addNegatedAttribute(partialObject));
        assertFalse(partialObjectDescription.addNegatedAttribute(partialObject));
    }

    @Test
    public void mtestPartialObjectDescription_containsNegatedAttributes() {
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        PartialObject partialObject = new PartialObject(fcaImplication);
        Set<String> set = new HashSet<>();
        partialObjectDescription.addNegatedAttribute(set);
        assertTrue(partialObjectDescription.containsNegatedAttribute(set));
        set.add("1");
        assertFalse(partialObjectDescription.containsNegatedAttribute(set));
    }

    @Test (expected = NullPointerException.class)
    public  void mtestChangeAttributeOrderAction_actionPerformed(){
        ChangeAttributeOrderAction changeAttributeOrderAction = new ChangeAttributeOrderAction();
        changeAttributeOrderAction.actionPerformed(null);
    }

    @Test (expected = NullPointerException.class)
    public  void mtestCounterExampleProvidedAction_actionPerformed(){
        FCAObject fcaObject = new FCAObject() {
            @Override
            public Object getIdentifier() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public ObjectDescription getDescription() {
                return null;
            }

            @Override
            public boolean respects(FCAImplication implication) {
                return false;
            }
        };
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        CounterExampleProvidedAction counterExampleProvidedAction = new CounterExampleProvidedAction(abstractContext,fcaImplication,fcaObject);
        counterExampleProvidedAction.putValue("A1", new Object());
        counterExampleProvidedAction.setContext(abstractContext);
        counterExampleProvidedAction.actionPerformed(new ActionEvent(new Object(), 0, "A1"));
        counterExampleProvidedAction.actionPerformed(null);
    }

    @Test (expected = NullPointerException.class)
    public  void mtestQuestionConfirmedAction_actionPerformed(){
        QuestionConfirmedAction questionConfirmedAction = new QuestionConfirmedAction();
        questionConfirmedAction.actionPerformed(null);
    }

    @Test (expected = NullPointerException.class)
    public  void mtestQuestionRejectedAction_actionPerformed(){
        QuestionRejectedAction questionRejectedAction = new QuestionRejectedAction();
        questionRejectedAction.actionPerformed(null);
    }

    @Test
    public  void mtestQResetExplorationAction_actionPerformed(){
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        ResetExplorationAction<String, String, FCAObject<String, String>> resetExplorationAction = Mockito.mock(ResetExplorationAction.class);
        resetExplorationAction.putValue("A1", new Object());
        resetExplorationAction.setContext(abstractContext);
        resetExplorationAction.actionPerformed(new ActionEvent(new Object(), 0, "A1"));
        resetExplorationAction.actionPerformed(null);
    }

    @Test (expected = NullPointerException.class)
    public  void mtestStartExplorationAction_actionPerformed(){
        StartExplorationAction startExplorationAction = new StartExplorationAction();
        startExplorationAction.actionPerformed(null);
    }

    @Test
    public void mtestNewImplicationChange(){
        NewImplicationChange newImplicationChange = new NewImplicationChange(null,null);
        assertNull(newImplicationChange.getImplication());
        assertTrue(newImplicationChange.getType() == 1);
    }

    @Test (expected = NullPointerException.class)
    public void mtestNewObjectChange(){
        NewObjectChange newObjectChange = new NewObjectChange(null,null);
        assertNull(newObjectChange.getObject());
        assertTrue(newObjectChange.getType() == 2);
        newObjectChange.undo();
    }

    @Test
    public void mtestObjectHasAttributeChange(){
        ObjectHasAttributeChange objectHasAttributeChange = new ObjectHasAttributeChange(null,null);
        assertNull(objectHasAttributeChange.getObject());
        assertNull(objectHasAttributeChange.getAttribute());
        assertTrue(objectHasAttributeChange.getType() == 0);
    }

    @Test (expected = NullPointerException.class)
    public void mtestLisetSet_addAll(){
        ListSet listSet = new ListSet();
        listSet.addAll(null);
    }

    @Test (expected = NullPointerException.class)
    public void mtestLisetSet_removeAll(){
        ListSet listSet = new ListSet();
        listSet.removeAll(null);
    }

    @Test (expected = NullPointerException.class)
    public void mtestLisetSet_retainAll(){
        ListSet listSet = new ListSet();
        assertFalse(listSet.retainAll(null));
        Object object = new Object();
        listSet.add(object);
        listSet.retainAll(null);
    }

    @Test (expected = NullPointerException.class)
    public void mtestLisetSet_toArray(){
        ListSet listSet = new ListSet();
        listSet.toArray(null);
    }

    @Test //(expected = NullPointerException.class)
    public void mtestLisetSet_getIndexOf(){
        ListSet listSet = new ListSet();
        assertTrue(listSet.getIndexOf(null) == -1);
        listSet.add("1");
        listSet.add("2");
        listSet.add("3");
        assertTrue(listSet.getIndexOf("2") == 1);
    }

    @Test (expected = NullPointerException.class)
    public void mtest_AbstractContext_getNextPremise(){
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        abstractContext.getNextPremise(null);
    }

    @Test (expected = NullPointerException.class)
    public void mtest_AbstractContext_continueExploration(){
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        abstractContext.continueExploration(null);
    }

    @Test (expected = NullPointerException.class)
    public void mtest_AbstractExpert_fireExpertAction(){
        AbstractExpert abstractExpert = new AbstractExpert() {
            @Override
            public void requestCounterExample(FCAImplication question) {

            }

            @Override
            public void askQuestion(FCAImplication question) {

            }

            @Override
            public void counterExampleInvalid(FCAObject counterExample, int reason) {

            }

            @Override
            public void forceToCounterExample(FCAImplication implication) {

            }

            @Override
            public void explorationFinished() {

            }

            @Override
            public void implicationFollowsFromBackgroundKnowledge(FCAImplication implication) {

            }
        };
        abstractExpert.addExpertActionListener(null);
        abstractExpert.fireExpertAction(null);
    }

    @Test //(expected = NullPointerException.class)
    public void mtest_FormalContext_addObject(){
        FormalContext formalContext = new FormalContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FullObject fullObject = new FullObject(fcaImplication);
        try{
            formalContext.addObject(fullObject);
        } catch (Exception IllegalObjectException) {
        }
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void mtest_FormalContext_getObjectAtIndex(){
        FormalContext formalContext = new FormalContext();
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FullObject fullObject = new FullObject(fcaImplication);
        formalContext.getObjectAtIndex(1);
    }

    @Test
    public void mtest_FormalContext_getObject(){
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FullObject fullObject = new FullObject(fcaImplication);
        FormalContext formalContext = new FormalContext();
        assertNull(formalContext.getObject(0));
        assertNull(formalContext.getObject(fullObject.getIdentifier()));
    }

    @Test
    public void mtest_FormalContext_removeObject(){
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FullObject fullObject = new FullObject(fcaImplication);
        FormalContext formalContext = new FormalContext();
        try{
            formalContext.removeObject(null);
        } catch (Exception  IllegalObjectException){
        }
        try {
            assertNull(formalContext.removeObject(fullObject.getIdentifier()));
        } catch (Exception  IllegalObjectException){
        }
    }

    @Test
    public void mtest_FormalContext_removeObject2(){
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FullObject fullObject = new FullObject(fcaImplication);
        FormalContext formalContext = new FormalContext();
        try{
            formalContext.removeObject(null);
        } catch (Exception  IllegalObjectException){
        }
        try {
            assertNull(formalContext.removeObject(fullObject));
        } catch (Exception  IllegalObjectException){
        }
    }

    @Test
    public void mtest_FormalContext_addAttributeToObject(){
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FullObject fullObject = new FullObject(fcaImplication);
        FormalContext formalContext = new FormalContext();
        try {
            formalContext.addAttributeToObject("1", 0);
        } catch (Exception IllegalObjectException) {
        }
    }

    @Test
    public void mtest_FormalContext_removeAttributeFromObject(){
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FormalContext formalContext = new FormalContext();
        try {
            formalContext.removeAttributeFromObject("1", 0);
        } catch (Exception IllegalObjectException) {
        }
    }

    @Test
    public void mtest_FormalContext_refutes(){
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FormalContext formalContext = new FormalContext();
        assertFalse(formalContext.refutes(fcaImplication));
        assertFalse(formalContext.refutes(null));
    }

    @Test (expected = NullPointerException.class)
    public void mtest_FullObject_refutes(){
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        FullObject fullObject = new FullObject(fcaImplication);
        fullObject.refutes(fcaImplication);
        fullObject.refutes(null);
    }

    @Test (expected = NullPointerException.class)
    public void mtest_ImplicationSet_add(){
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        ImplicationSet implicationSet = new ImplicationSet(abstractContext);
        FCAImplication fcaImplication = new FCAImplication() {
            @Override
            public Set getPremise() {
                return null;
            }

            @Override
            public Set getConclusion() {
                return null;
            }

            @Override
            public boolean equals(FCAImplication imp) {
                return false;
            }
        };
        implicationSet.add(fcaImplication);
    }

    @Test
    public void mtest_ImplicationSet_closure(){
        AbstractContext abstractContext = new AbstractContext() {
            @Override
            public IndexedSet getObjects() {
                return null;
            }

            @Override
            public boolean addObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(Object id) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean removeObject(FCAObject object) throws IllegalObjectException {
                return false;
            }

            @Override
            public boolean addAttributeToObject(Object attribute, Object id) throws IllegalAttributeException, IllegalObjectException {
                return false;
            }

            @Override
            public Set doublePrime(Set x) {
                return null;
            }

            @Override
            public Set<FCAImplication> getStemBase() {
                return null;
            }

            @Override
            public boolean refutes(FCAImplication imp) {
                return false;
            }

            @Override
            public boolean isCounterExampleValid(FCAObject counterExample, FCAImplication imp) {
                return false;
            }

            @Override
            protected boolean followsFromBackgroundKnowledge(FCAImplication implication) {
                return false;
            }

            @Override
            public FCAObject getObject(Object id) {
                return null;
            }

            @Override
            public FCAObject getObjectAtIndex(int index) {
                return null;
            }

            @Override
            public boolean objectHasAttribute(FCAObject object, Object attribute) {
                return false;
            }

            @Override
            public Set<FCAImplication> getDuquenneGuiguesBase() {
                return null;
            }

            @Override
            public Expert getExpert() {
                return null;
            }

            @Override
            public void setExpert(Expert e) {

            }
        };
        ImplicationSet implicationSet = new ImplicationSet(abstractContext);
        Set <FCAImplication> set = new HashSet<>();
        implicationSet.closure(set);
    }

    @Test (expected = NullPointerException.class)
    public void mtest_PartialObjectDescription_containsNegatedAttributes(){
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        Set <String> set = new HashSet<>();
        set.add("1");
        set.add("2");
        partialObjectDescription.addNegatedAttribute("1");
        partialObjectDescription.addNegatedAttribute("2");
        assertTrue(partialObjectDescription.containsNegatedAttributes(set));
        set.add("3");
        assertFalse(partialObjectDescription.containsNegatedAttributes(set));
        partialObjectDescription.containsNegatedAttributes(null);
    }

    @Test (expected = NullPointerException.class)
    public void mtest_StartExplorationAction_actionPerformed(){
        StartExplorationAction startExplorationAction = new StartExplorationAction();
        ActionEvent actionEvent = new ActionEvent(new Object(), 0, "A1");
        startExplorationAction.actionPerformed(actionEvent);
        startExplorationAction.actionPerformed(null);
    }
}
